require 'rails_helper'

describe LateTaskNotificationCreator do
  describe '#call' do
    it 'creates notifications' do
      task_1 = create(:task, ending_date: 2.days.ago)
      task_2 = create(:task, ending_date: 1.days.ago)
      task_3 = create(:task, ending_date: 1.days.ago)

      task_1.contributors << task_1.owner
      task_2.contributors << task_2.owner
      task_3.contributors << task_3.owner

      tasks = [task_1, task_2, task_3 ]

      expect { described_class.new.run tasks }
        .to change { Notification.count }.by 3
    end

    it 'generates notifications for task owners' do
      task = create(:task, ending_date: 2.days.ago)
      task.contributors << task.owner

      described_class.new.run [task]

      expect(Notification.last.user).to eq task.owner
    end

    it 'generates notifications for contributors' do
      user = create(:user)
      task = create(:task)
      task.project.contributors << user
      task.contributors << user
      task.contributors << task.owner

      expect { described_class.new.run([task]) }
        .to change { Notification.count }.by 2

      expect(Notification.last(2).map(&:user))
        .to contain_exactly(task.owner, user)
    end
  end
end
