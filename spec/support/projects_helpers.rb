# frozen_string_literal: true
module ProjectsHelpers
  def params_for_project
    { project: {
      name: 'foo',
      starting_date: Time.zone.now + 1.hour,
      ending_date: Time.zone.now + 1.day
    } }
  end
end
