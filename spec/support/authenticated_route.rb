# frozen_string_literal: true
RSpec.shared_examples 'authenticated route' do |method, *args|
  it 'should authenticate user' do
    sign_in nil

    send(method, *args)
    expect(response).to redirect_to(new_user_session_path)
  end

  it 'allows signed in users' do
    @user = create(:user)

    sign_in @user

    send(method, *args)
    expect(response.status).to_not eq :unauthorized
  end
end
