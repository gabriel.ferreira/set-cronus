# frozen_string_literal: true
FactoryGirl.define do
  sequence(:username) { |n| "someone#{n}" }
  sequence(:project_name) { |n| "Project-#{n}" }
  sequence(:task_name) { |n| "Task-#{n}" }
  sequence(:notification_description) do |n|
    "Some useful description about some event that occured #{n.days.ago}, " \
      "triggered by user #{n}"
  end
end
