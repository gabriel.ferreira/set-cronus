# frozen_string_literal: true
FactoryGirl.define do
  factory :project do
    name	{ generate :project_name }
    starting_date	{ Time.zone.now + 1.minute }
    ending_date	{ starting_date + 10.days }
    user
  end
end
