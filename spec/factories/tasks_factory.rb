# frozen_string_literal: true
FactoryGirl.define do
  factory :task do
    name { generate(:task_name) }
    description 'some description goes here'
    starting_date { Time.zone.now + 1.day }
    ending_date { starting_date + 1.day }
    project
  end
end
