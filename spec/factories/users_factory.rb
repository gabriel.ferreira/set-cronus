# frozen_string_literal: true
FactoryGirl.define do
  factory :user do
    name { generate :username }
    email { "#{name}@example.com" }
    password 'sample_mail'
  end
end
