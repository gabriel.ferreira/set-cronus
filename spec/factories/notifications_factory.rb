# frozen_string_literal: true
FactoryGirl.define do
  factory :notification do
    description { generate :notification_description }
    notification_type 'random'
    seen false
    project
    task
    user

    trait :status_changed do
      notification_type 'status_changed'
    end

    trait :task_finished do
      notification_type 'task_finished'
    end
  end
end
