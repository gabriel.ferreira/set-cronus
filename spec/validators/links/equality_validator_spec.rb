# frozen_string_literal: true
require 'rails_helper'

describe Links::EqualityValidator do
  let(:task) { build(:task) }

  subject { described_class.new(task: task, dependency: dependency) }

  describe '#errors' do
    context 'when both are different' do
      let(:dependency) { build(:task) }

      it 'sets ad valid' do
        expect(subject.errors).to eq({})
      end
    end

    context 'when both are equal' do
      let(:dependency) { task }

      it 'sets as invalid' do
        expect(subject.errors).to eq(equality: ['Cannot link a task to itself'])
      end
    end
  end
end
