# frozen_string_literal: true
require 'rails_helper'

describe Links::ModelValidator do
  subject { described_class.new(task: task, dependency: dependency) }

  let(:task) { build(:task) }
  let(:dependency) { build(:task) }

  describe '#errors' do
    context 'when both are valid' do
      it 'sets as valid' do
        expect(subject.errors).to eq({})
      end
    end

    context 'when task is invalid' do
      let!(:task) { build(:task) }

      it 'sets as invalid' do
        allow(task).to receive(:valid?)
          .and_return false

        error_array = subject.errors

        expect(error_array).to_not be_empty
        expect(error_array).to match(model: ['One of the given tasks is invalid'])
      end
    end

    context 'when dependency is invalid' do
      let!(:dependency) { build(:task) }

      it 'sets as valid' do
        allow(dependency).to receive(:valid?)
          .and_return false

        error_array = subject.errors

        expect(error_array).to_not be_empty
        expect(error_array).to match(model: ['One of the given tasks is invalid'])
      end
    end

    context 'when both are invalid' do
      let(:task) { build(:task) }
      let(:dependency) { build(:task) }

      it 'sets as valid' do
        allow(task).to receive(:valid?)
          .and_return false

        allow(dependency).to receive(:valid?)
          .and_return false

        error_array = subject.errors

        expect(error_array.count).to eq 1
      end
    end
  end
end
