# frozen_string_literal: true
require 'rails_helper'

describe Links::DependenciesValidator do
  let(:task) { build(:task) }

  subject { described_class.new(task: task, dependency: dependency) }

  describe '#errors' do
    context 'when valid' do
      let(:dependency) { build(:task, project: task.project) }

      it 'sets as valid' do
        expect(subject.errors).to eq({})
      end
    end

    context 'when direct dependency circle' do
      let(:dependency) { build(:task, project: task.project) }

      it 'sets as invalid' do
        dependency.dependencies << task

        expect(subject.errors).to eq(dependency: ['Would create a dependency circle'])
      end
    end

    context 'when indirect dependency circle' do
      let(:task_b) { build(:task, project: task.project) }
      let(:dependency) { build(:task, project: task.project) }

      it 'sets as invalid' do
        dependency.dependencies << task_b
        task_b.dependencies << task

        expect(subject.errors).to eq(dependency: ['Would create a dependency circle'])
      end
    end
  end
end
