# frozen_string_literal: true
require 'rails_helper'

describe Links::SameProjectValidator do
  subject { described_class.new(task: task, dependency: dependency) }

  let(:task) { build(:task) }

  describe '#errors' do
    context 'when task and dependency belong to same project' do
      let(:dependency) { build(:task, project: task.project) }

      it 'sets as valid' do
        expect(subject.errors).to eq({})
      end
    end

    context 'when task and dependency do not belong to same project' do
      let(:dependency) { build(:task) }

      it 'sets as valid' do
        expect(subject.errors).to eq(project: ['Belongs to different projects'])
      end
    end
  end
end
