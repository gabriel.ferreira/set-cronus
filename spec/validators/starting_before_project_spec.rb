require 'rails_helper'

describe StartingBeforeProject do
  before do
    @starting_date = Time.zone.now.beginning_of_day
    @ending_date = Time.zone.now.beginning_of_day + 1.month
    @project = build(:project, starting_date: @starting_date,
                               ending_date: @ending_date)
    @errors = build(:task).errors
  end

  describe '#validate' do
    context 'when task starts before project starting date' do
      it 'points an error' do
        subject = described_class.new(project: @project,
                                      starting_date: @starting_date - 1.day,
                                      ending_date: @ending_date,
                                      errors: @errors)

        subject.validate
        expect(@errors[:starting_date]).to include 'The task cannot start before the project'
      end
    end

    context 'when task starts after project starting date' do
      it 'does not points an error' do
        subject = described_class.new(project: @project,
                                      starting_date: @starting_date + 1.day,
                                      ending_date: @ending_date,
                                      errors: @errors)

        subject.validate
        expect(@errors.messages).to be_empty
      end
    end

    context 'when task starts at same day of project starting date' do
      it 'does not points an error' do
        subject = described_class.new(project: @project,
                                      starting_date: @starting_date + 1.hour,
                                      ending_date: @ending_date,
                                      errors: @errors)

        subject.validate
        expect(@errors.messages).to be_empty
      end
    end

    context 'when task starts at same day of project ending date' do
      it 'does not points an error' do
        subject = described_class.new(project: @project,
                                      starting_date: @ending_date,
                                      ending_date: @ending_date,
                                      errors: @errors)

        subject.validate
        expect(@errors.messages).to be_empty
      end
    end

    context 'when task starts after the project ending date' do
      it 'does not points an error' do
        subject = described_class.new(project: @project,
                                      starting_date: @ending_date + 1.day,
                                      ending_date: @ending_date,
                                      errors: @errors)

        subject.validate
        expect(@errors.messages).to be_empty
      end
    end

    context 'when task starts during the project deadline' do
      it 'does not points an error' do
        subject = described_class.new(project: @project,
                                      starting_date: @ending_date - 1.day,
                                      ending_date: @ending_date,
                                      errors: @errors)

        subject.validate
        expect(@errors.messages).to be_empty
      end
    end
  end
end
