require 'rails_helper'

describe StartingDateBeforeToday do
  before { @errors = build(:project).errors }

  describe '#validate' do
    context 'when starting date is greater than current time' do
      it 'returns empty hash' do
        subject = described_class.new(starting_date: 1.day.from_now,
                                      name: 'project',
                                      errors: @errors)
        subject.validate
        expect(@errors[:starting_date]).to be_empty
      end
    end

    context 'when starting date is lower than current time' do
      it 'returns non empty hash' do
        subject = described_class.new(name: 'project', starting_date: 1.day.ago, errors: @errors)
        subject.validate
        expect(@errors[:starting_date])
          .to include('The starting date must be after today')
      end
    end

    context 'when starting date is today' do
      it 'returns empty hash' do
        time = Time.zone.now.beginning_of_day
        subject = described_class.new(starting_date: time,
                                      name: 'project',
                                      errors: @errors)
        subject.validate
        expect(@errors[:starting_date]).to be_empty
      end
    end

    context 'when starting date is one minute less than beginning of today' do
      it 'returns non empty hash' do
        time = Time.zone.now.beginning_of_day - 1.minute
        subject = described_class.new(starting_date: time,
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:starting_date])
          .to include('The starting date must be after today')
      end
    end

    context 'when starting date is one minute more than beginning of today' do
      it 'returns non empty hash' do
        time = Time.zone.now.beginning_of_day + 1.minute
        subject = described_class.new(starting_date: time,
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:starting_date]).to be_empty
      end
    end
  end
end