require 'rails_helper'

describe EndingBeforeStarting do
  before { @errors = build(:project).errors }

  describe '#validate' do
    context 'when starting date is lower than ending date' do
      it 'does not point errors' do
        starting_date = Time.zone.now

        subject = described_class.new(starting_date: starting_date,
                                      name: 'project',
                                      errors: @errors,
                                      ending_date: starting_date + 1.day)

        subject.validate
        expect(@errors[:ending_date]).to be_empty
      end
    end

    context 'when starting date is greater than ending date' do
      it 'point errors' do
        starting_date = Time.zone.now

        subject = described_class.new(starting_date: starting_date,
                                      name: 'project',
                                      errors: @errors,
                                      ending_date: starting_date - 1.day)

        subject.validate
        expect(@errors[:ending_date]).to include 'Project cannot finish before has started'
      end
    end

    context 'when starting date is greater than ending date by one minute' do
      it 'point errors' do
        starting_date = Time.zone.now

        subject = described_class.new(starting_date: starting_date,
                                      name: 'project',
                                      errors: @errors,
                                      ending_date: starting_date - 1.minute)

        subject.validate
        expect(@errors[:ending_date]).to include 'Project cannot finish before has started'
      end
    end

    context 'when starting date is equal to ending date' do
      it 'point errors' do
        starting_date = Time.zone.now

        subject = described_class.new(starting_date: starting_date,
                                      name: 'project',
                                      errors: @errors,
                                      ending_date: starting_date)

        subject.validate
        expect(@errors[:ending_date]).to include 'Project cannot start and end at the same day'
      end
    end

    context 'when starting date and ending date are the same day' do
      it 'point errors' do
        starting_date = Time.zone.now

        subject = described_class.new(starting_date: starting_date,
                                      name: 'project',
                                      errors: @errors,
                                      ending_date: starting_date + 2.hours)

        subject.validate
        expect(@errors[:ending_date]).to include 'Project cannot start and end at the same day'
      end
    end
  end
end
