require 'rails_helper'

describe EndingDateBeforeToday do
  before { @errors = build(:project).errors }

  describe '#validate' do
    context 'when ending date is greater than current time' do
      it 'returns empty hash' do
        subject = described_class.new(ending_date: 1.day.from_now,
                                      name: 'project',
                                      errors: @errors)
        subject.validate
        expect(@errors[:ending_date]).to be_empty
      end
    end

    context 'when ending date is lower than current time' do
      it 'returns non empty hash' do
        subject = described_class.new(ending_date: 1.day.ago, name: 'project', errors: @errors)
        subject.validate
        expect(@errors[:ending_date])
          .to include('The ending date must be after today')
      end
    end

    context 'when ending date is today' do
      it 'returns empty hash' do
        time = Time.zone.now.beginning_of_day
        subject = described_class.new(ending_date: time,
                                      name: 'project',
                                      errors: @errors)
        subject.validate
        expect(@errors[:ending_date]).to be_empty
      end
    end

    context 'when ending date is one minute less than beginning of today' do
      it 'returns non empty hash' do
        time = Time.zone.now.beginning_of_day - 1.minute
        subject = described_class.new(ending_date: time,
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:ending_date])
          .to include('The ending date must be after today')
      end
    end

    context 'when ending date is one minute more than beginning of today' do
      it 'returns non empty hash' do
        time = Time.zone.now.beginning_of_day + 1.minute
        subject = described_class.new(ending_date: time,
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:ending_date]).to be_empty
      end
    end
  end
end
