require 'rails_helper'

describe MissingDeadlines do
  before { @errors = build(:project).errors }

  describe '#validate' do
    context 'when starting date and ending date are given' do
      it 'does not point errors' do
        subject = described_class.new(starting_date: Time.zone.now.to_s,
                                      ending_date: 5.days.from_now.to_s,
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors.full_messages).to be_empty
      end
    end

    context 'when ending date is missing' do
      it 'point errors' do
        subject = described_class.new(starting_date: Time.zone.now.to_s,
                                      ending_date: '',
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:ending_date]).to include 'Ending date is missing'
      end
    end

    context 'when starting date is missing' do
      it 'point errors' do
        subject = described_class.new(starting_date: '',
                                      ending_date: Time.zone.now.to_s,
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:starting_date]).to include 'Starting date is missing'
      end
    end

    context 'when both values are missing' do
      it 'point errors' do
        subject = described_class.new(starting_date: '',
                                      ending_date: '',
                                      name: 'project',
                                      errors: @errors)

        subject.validate
        expect(@errors[:starting_date]).to include 'Starting date is missing'
        expect(@errors[:ending_date]).to include 'Ending date is missing'
      end
    end
  end
end
