# frozen_string_literal: true
require 'rails_helper'

describe ProjectsController, type: :controller do
  render_views

  describe 'new' do
    it_behaves_like 'authenticated route', :get, 'new', params: { user_id: 1 }

    it 'renders new template' do
      sign_in create(:user)

      get 'new', params: { user_id: 1 }
      expect(response).to render_template('projects/new')
    end
  end

  describe 'create' do
    it_behaves_like 'authenticated route', :post, 'create',
                    params: { user_id: 1,
                              project: {
                                name: 'foo',
                                starting_date: Time.zone.now + 1.hour,
                                ending_date: Time.zone.now + 1.day
                              } }

    it 'renders show template for successful saves' do
      user = create(:user)
      sign_in user

      expect do
        post 'create', params: { user_id: 1 }.merge(params_for_project)
      end.to change { user.projects.count }.by 1
    end
  end

  describe 'show' do
    it 'renders 200 when found' do
      project = create(:project)
      sign_in project.user

      get :show, params: { user_id: 1, id: project.id }

      expect(response).to render_template('projects/show')
    end
  end
end
