require 'rails_helper'

describe TaskAcceptUpdater do
  before do
    @user = create(:user)

    @task = create(:task, state: 'verifying')
    @task.project.contributors << @user
    @task.contributors << @user
  end

  describe 'task side effects' do
    it 'is updated to finished' do
      described_class.new(@task, @task.owner).update
      expect(@task.reload.state).to eq 'finished'
    end

    it 'stores current time in finished_at' do
      described_class.new(@task, @task.owner).update
      finished_at = @task.reload
        .finished_at
        .strftime("%d-%m-%Y %H:%M:%S")

      expect(finished_at).to eq Time.zone.now.strftime("%d-%m-%Y %H:%M:%S")
    end
  end

  describe 'state change reports generated' do
    it 'generates a state change report for task owner' do
      described_class.new(@task, @task.owner).update

      last_report = TaskReport.last

      expect(last_report.user).to eq @task.owner
      expect(last_report.task).to eq @task
      expect(last_report.project).to eq @task.project
      expect(last_report.original_state).to eq 'verifying'
      expect(last_report.new_state).to eq 'finished'
    end

    it 'generates a state change report for any user' do
      other_user = create(:user)
      described_class.new(@task, other_user).update

      last_report = TaskReport.last

      expect(last_report.user).to eq other_user
      expect(last_report.task).to eq @task
      expect(last_report.project).to eq @task.project
      expect(last_report.original_state).to eq 'verifying'
      expect(last_report.new_state).to eq 'finished'
    end
  end

  describe 'finish reports generated' do
    it 'generates a finish report' do
      described_class.new(@task, @task.owner).update

      last_report = TaskFinishReport.last

      expect(last_report.user).to eq @task.owner
      expect(last_report.task).to eq @task
      expect(last_report.project).to eq @task.project
      expect(last_report.delayed_time).to be_a Integer
    end
  end
end
