require 'rails_helper'

describe TaskScheduleUpdater do
	describe '#update' do
		context 'when without dependencies' do
			before do
				@task = create(:task, state: 'scheduled')
			end

			it 'updates task state to ready' do
				expect_any_instance_of(StateMachines::Work)
					.to receive(:ready)

        described_class.new(@task, @task.owner).update
			end
		end

		context 'when has dependencies' do
			it 'updates task to ready if dependencies are finished' do
				@task = create(:task, state: 'scheduled')
				@dependencies = create_list(:task, 2, state: 'finished')
				@task.dependencies << @dependencies

				expect_any_instance_of(StateMachines::Work)
					.to receive(:ready)

        described_class.new(@task, @task.owner).update
			end

			it 'updates task to scheduled if dependencies are not finished' do
				@task = create(:task, state: 'discarded')
				@dependency_1 = create(:task, state: 'finished')
				@dependency_2 = create(:task, state: 'started')
				@task.dependencies << [@dependency_1, @dependency_2]

				expect_any_instance_of(StateMachines::Work)
					.to receive(:schedule)

        described_class.new(@task, @task.owner).update
			end
		end
	end
end
