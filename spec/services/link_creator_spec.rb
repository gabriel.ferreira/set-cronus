require 'rails_helper'

describe LinkCreator do
	let!(:dependency) { create(:task, state: 'ready') }
	let!(:requisite) { create(:task, state: 'ready') }
	let(:validator) { double }

	let(:params) do
		{ requisite: requisite,
			dependency: dependency,
      user: requisite.owner,
			validator: validator }
	end

	subject { described_class.new params }

	before do
		allow(validator)
			.to receive(:valid?)
			.and_return true
	end

	describe '#create' do
		it 'links tasks with given ids' do
			subject.create

			expect(requisite.reload.dependencies)
			  .to include dependency
		end

		it 'keeps requisite status as ready' do
			expect { subject.create }
				.to_not change { requisite.reload.state }
				.from('ready')
		end

		context 'when dependency has status ready' do
			it 'updates dependency status from ready to scheduled' do
				expect { subject.create }
					.to change { dependency.reload.state }
					.from('ready')
					.to('scheduled')
			end
		end

		context 'when dependency has status started' do
			let!(:dependency) { create(:task, state: 'started') }

			it 'updates dependency status from started to stopped' do
				expect { subject.create }
					.to change { dependency.reload.state }
					.from('started')
					.to('stopped')
			end
		end

		context 'when dependency has status finished' do
			let!(:dependency) { create(:task, state: 'finished') }

			it 'do not perform linking' do
				subject.create
				expect(requisite.reload.dependencies)
					.to_not include dependency
			end
		end

		context 'when the linking is invalid' do
			before do
				allow(validator)
					.to receive(:valid?)
					.and_return false
			end

			it 'does not links the referred tasks' do
				subject.create
				expect(requisite.reload.dependencies)
					.to_not include dependency
			end

			it 'does not update dependency state' do
				expect { subject.create }
					.to_not change { dependency.reload.state }
					.from('ready')
			end
		end
	end
end
