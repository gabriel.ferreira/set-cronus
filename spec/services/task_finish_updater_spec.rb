require 'rails_helper'

describe TaskFinishUpdater do
  describe '#update' do
    before do
      @task = create(:task, state: 'started')
    end

    it 'sets given task to finished' do
      described_class.new(@task, @task.owner).update

      expect(@task.reload.state).to eq 'finished'
    end

    it 'updates dependencies ready to start' do
      expect_any_instance_of(DependencyReadyUpdater)
        .to receive(:update)

      described_class.new(@task, @task.owner).update
    end

    it 'triggers callbacks' do
      expect_any_instance_of(StateMachines::Callbacks::TaskFinishedAt)
        .to receive(:run)

      expect_any_instance_of(StateMachines::Callbacks::TaskFinishReports)
        .to receive(:run)

      expect_any_instance_of(StateMachines::Callbacks::FinishNotifications)
        .to receive(:run)

      described_class.new(@task, @task.owner).update
    end
  end
end
