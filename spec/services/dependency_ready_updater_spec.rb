require 'rails_helper'

describe DependencyReadyUpdater do
	context 'when has dependencies' do
		it 'updates dependency to ready if do not has dependencies' do
			@task = create(:task, state: 'finished')
			@task_dependents = [create(:task, state: 'scheduled'),
													create(:task, state: 'scheduled')]
			@task.dependencies << @task_dependents

      described_class.new(@task_dependents, @task.owner).update

			@task_dependents.each do |dependent|
				expect(dependent.reload.state).to eq 'ready'
			end
		end

		it 'do not updates dependents that have dependencies not finished yet' do
			@task = create(:task, state: 'finished')
			@another_task = create(:task, state: 'started')
			@task_dependents = [create(:task, state: 'scheduled'),
													create(:task, state: 'scheduled')]

			@task.dependencies << @task_dependents
			@another_task.dependencies << @task_dependents.last

			described_class.new(@task_dependents, @task.owner).update

			expect(@task_dependents.last.reload.state)
				.to eq 'scheduled'
		end

		it 'do not update dependents dependents' do
			@task = create(:task, state: 'finished')
			@task_dependents = [create(:task, state: 'scheduled')]
			@dependent_dependent = create(:task, state: 'scheduled')

			@task.dependencies << @task_dependents
			@task_dependents.first.dependencies << @dependent_dependent

			described_class.new(@task_dependents, @task.owner).update

			expect(@dependent_dependent.reload.state).to eq 'scheduled'
		end

		it 'do not update if task is not finished' do
			@task = create(:task, state: 'started')
			@task_dependents = [create(:task, state: 'scheduled')]

			@task.dependencies << @task_dependents

			described_class.new(@task_dependents, @task.owner).update

			expect(@task_dependents.first.reload.state)
				.to eq 'scheduled'
		end
	end
end
