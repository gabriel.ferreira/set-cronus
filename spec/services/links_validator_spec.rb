# frozen_string_literal: true
require 'rails_helper'

describe LinksValidator do
  let(:task) { build(:task) }
  let(:dependency) { build(:task, project: task.project) }

  subject { described_class.new(task, dependency) }

  describe '#errors' do
    let(:valid_task) { build(:task, project: project) }

    it 'returns empty for valid tasks' do
      expect(subject.errors)
        .to match({})
    end

    described_class::VALIDATORS.each do |validator|
      it "runs #{validator}.errors" do
        expect_any_instance_of(validator)
          .to receive(:errors)
          .and_call_original

        subject.errors
      end
    end
  end
end
