# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Work do
  subject { described_class.new(record: task, user: task.owner) }
  let(:task) { build(:task) }

  StateMachines::WorkEvents::EVENTS.each do |event|
    describe "##{event}" do
      it "calls #{event} event" do
        klass = eval("StateMachines::Events::#{event.capitalize}")
        expect_any_instance_of(klass)
          .to receive(:change_state)

        subject.send(event.to_sym)
      end
    end
  end
end
