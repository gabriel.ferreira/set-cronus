require 'rails_helper'

describe StateMachines::AllowedTransitions do
	let(:task) { build(:task, state: state) }
  subject { described_class.new(record: task, user: task.owner) }

	describe '#allowed_transitions' do
		context 'when state is scheduled' do
			let(:state) { 'scheduled' }

			it 'returns allowed transitions' do
				expect(subject.allowed_transitions)
				  .to contain_exactly 'discard', 'ready'
			end
		end

		context 'when state is ready' do
			let(:state) { 'ready' }

			it 'returns allowed transitions' do
				expect(subject.allowed_transitions)
				  .to contain_exactly 'discard', 'start', 'schedule'
			end
		end

		context 'when state is started' do
			let(:task) { build(:task, state: 'started') }

			it 'when project is single-user returns allowed transitions' do
				expect(subject.allowed_transitions)
				  .to contain_exactly 'stop', 'finish', 'ready', 'schedule'
			end

			it 'when project is multi-user returns allowed transitions' do
				task.project.contributors << create(:user)
        instance = described_class.new(record: task, user: task.owner)

				expect(instance.allowed_transitions)
				  .to contain_exactly 'stop', 'verify', 'ready', 'schedule'
			end
		end

		context 'when state is stopped' do
			let(:state) { 'stopped' }

			it 'returns allowed transitions' do
				expect(subject.allowed_transitions)
				  .to contain_exactly 'continue', 'discard'
			end
		end

		context 'when state is finished' do
			let(:state) { 'finished' }

			it 'returns allowed transitions' do
				expect(subject.allowed_transitions)
				  .to be_empty
			end
		end

		context 'when state is verifying' do
			let(:state) { 'verifying' }

			it 'returns allowed transitions' do
				task.project.contributors << create(:user)
        instance = described_class.new(record: task, user: task.owner)

				expect(instance.allowed_transitions)
				  .to contain_exactly 'reject', 'accept', 'discard'
			end
		end

		context 'when state is discarded' do
			let(:state) { 'discarded' }

			it 'returns allowed transitions' do
				expect(subject.allowed_transitions)
				  .to contain_exactly 'schedule', 'ready'
			end
		end
	end
end
