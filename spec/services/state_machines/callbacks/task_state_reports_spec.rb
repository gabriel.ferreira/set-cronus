require 'rails_helper'

describe StateMachines::Callbacks::TaskStateReports do
  before do
    @task = create(:task, state: 'scheduled')
  end

  describe '#run' do
    it 'records a state change' do
      expect do
        described_class
          .new(record: @task,
               original_state: @task.state,
               new_state: 'ready',
               user: @task.owner).run
      end.to change { TaskReport.count }.by 1

      report = TaskReport.last

      expect(report.project).to eq @task.project
      expect(report.task).to eq @task
      expect(report.user).to eq @task.owner
      expect(report.original_state).to eq 'scheduled'
      expect(report.new_state).to eq 'ready'
    end
  end
end
