require 'rails_helper'

describe StateMachines::Callbacks::FinishNotifications do
  before do
    @task = create(:task, state: 'scheduled')
    @user = create(:user)
  end

  describe '#run' do
    it 'notificates a state change' do
      expect do
        described_class
          .new(record: @task,
               original_state: @task.state,
               new_state: 'ready',
               user: @user).run
      end.to change { Notification.count }.by 1

      notification = Notification.last
      now = I18n.l notification.created_at, format: :long

      expect(notification.project).to eq @task.project
      expect(notification.task).to eq @task
      expect(notification.user).to eq @task.owner
      expect(notification.seen?).to eq false
      expect(notification.notification_type).to eq 'task_finished'
      expect(notification.description)
        .to eq "The user #{@user.name} has finished " \
               "the task #{@task.name} at #{now}"
    end
  end
end
