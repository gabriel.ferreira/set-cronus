require 'rails_helper'

describe StateMachines::Callbacks::TaskStartedAt do
	describe '#run' do
	  context 'when was not started yet' do
	    it 'sets started_at field with current time zone' do
	    	task = build(:task, started_at: nil)
	    	now = Time.zone.now

	      allow(Time.zone)
	        .to receive(:now)
	        .and_return now

	      described_class.new(record: task).run

	      expect(task.started_at.to_i)
	        .to eq now.to_i
	    end
	  end

	  context 'when it was already started before' do
	    it 'does not overwrite last data' do
	    	task = build(:task, started_at: Time.zone.now)

	      expect { described_class.new(record: task).run }
	        .to_not change { task.started_at }
		  end
	  end
  end
end
