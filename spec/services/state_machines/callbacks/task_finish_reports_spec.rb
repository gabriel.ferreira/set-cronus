require 'rails_helper'

describe StateMachines::Callbacks::TaskFinishReports do
  before do
    @task = create(:task, state: 'scheduled')
  end

  describe '#run' do
    it 'creates a task finish report record' do
      expect do
        described_class
          .new(record: @task,
               user: @task.owner).run
      end.to change { TaskFinishReport.count }.by 1

      report = TaskFinishReport.last

      expect(report.project).to eq @task.project
      expect(report.task).to eq @task
      expect(report.user).to eq @task.owner
    end

    context 'when task was finished 10 days before ending day' do
      it 'stores delayed time as -10' do
        @task.update_attributes(ending_date: 10.days.ago)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq -10
      end
    end

    context 'when task was finished 1 day before ending day' do
      it 'stores delayed time as -1' do
        @task.update_attributes(ending_date: 1.day.ago)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq -1
      end
    end

    context 'when task was finished with low difference time' do
      it 'stores delayed time as 0' do
        @task.update_attributes(ending_date: Time.zone.now)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end

    context 'when task was finished one day after the ending date' do
      it 'stores delayed time as 1' do
        @task.update_attributes(ending_date: 1.day.from_now)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 1
      end
    end

    context 'when task was finished ten days after the ending date' do
      it 'stores delayed time as 10' do
        @task.update_attributes(ending_date: 10.days.from_now)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 10
      end
    end

    context 'when task was finished a few hours before ending date' do
      it 'stores delayed time as 0' do
        @task.update_attributes(ending_date: Time.zone.now + 3.hours)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end

    context 'when task was finished a few hours after ending date' do
      it 'stores delayed time as 0' do
        @task.update_attributes(ending_date: Time.zone.now - 3.hours)

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end

    context 'when task was finished few hours after the beginning of the ending day' do
      it 'stores delayed time as 1' do
        @task.update_attributes(ending_date: Time.zone.today.beginning_of_day)

        allow(Time.zone)
          .to receive(:now)
          .and_return Time.zone.today.beginning_of_day - 2.hours

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end

    context 'when task was finished few hours before the beginning of the ending day' do
      it 'stores delayed time as 1' do
        @task.update_attributes(ending_date: Time.zone.today.beginning_of_day)

        allow(Time.zone)
          .to receive(:now)
          .and_return Time.zone.today.beginning_of_day + 2.hours

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end

    context 'when task was finished few hours after the ending of the ending date' do
      it 'stores delayed time as 1' do
        @task.update_attributes(ending_date: Time.zone.today.end_of_day)

        allow(Time.zone)
          .to receive(:now)
          .and_return Time.zone.today.end_of_day - 2.hours

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end

    context 'when task was finished few hours before the ending of the ending date' do
      it 'stores delayed time as 1' do
        @task.update_attributes(ending_date: Time.zone.today.end_of_day)

        allow(Time.zone)
          .to receive(:now)
          .and_return Time.zone.today.end_of_day + 2.hours

        described_class.new(record: @task, user: @task.owner).run

        expect(TaskFinishReport.last.delayed_time)
          .to eq 0
      end
    end
  end
end
