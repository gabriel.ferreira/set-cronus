require 'rails_helper'

describe StateMachines::Callbacks::TaskFinishedAt do
	describe '#run' do
		it 'sets finished_at field with current time zone' do
			task = build(:task, finished_at: nil)
			now = Time.zone.now

			allow(Time.zone)
				.to receive(:now)
				.and_return now

			described_class.new(record: task).run

			expect(task.finished_at.to_i)
				.to eq now.to_i
		end
	end
end
