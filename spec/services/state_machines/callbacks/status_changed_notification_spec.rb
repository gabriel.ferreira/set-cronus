require 'rails_helper'

describe StateMachines::Callbacks::StatusChangedNotification do
  before do
    @task = create(:task, state: 'scheduled')
  end

  describe '#run' do
    it 'notificates a state change' do
      expect do
        described_class
          .new(record: @task,
               original_state: @task.state,
               new_state: 'ready',
               user: @task.owner).run
      end.to change { Notification.count }.by 1

      notification = Notification.last

      expect(notification.project).to eq @task.project
      expect(notification.task).to eq @task
      expect(notification.user).to eq @task.owner
      expect(notification.seen?).to eq false
      expect(notification.notification_type).to eq 'status_changed'
      expect(notification.description)
        .to eq "The user #{@task.owner.name} has changed " \
               "#{@task.name} state from scheduled to ready " \
               "at #{I18n.l notification.created_at, format: :long}"
    end
  end
end
