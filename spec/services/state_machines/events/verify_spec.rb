# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Verify do
  context 'when project is multi-user' do
    before do
      @task = create(:task)
      @task.project.contributors << create(:user)
    end

    it 'when task is started can be set as verifying' do
      @task.update_attributes(state: 'started')
      subject = described_class.new(record: @task, user: @task.owner)

      expect { subject.change_state }
        .to change { @task.state }
        .from('started')
        .to('verifying')
    end

    it 'when task is anything else cannot be set as finished' do
      @task.update_attributes(state: 'anything')
      subject = described_class.new(record: @task, user: @task.owner)

      expect { subject.change_state }
        .to_not change { @task.reload.state }
        .from('anything')
    end
  end

  context 'when project is single user' do
    it 'when task is started cannot be set as verifying' do
      @task = create(:task, state: 'started')
      subject = described_class.new(record: @task, user: @task.owner)

      expect { subject.change_state }
        .to_not change { @task.reload.state }
        .from('started')
    end

    it 'when task is anything else cannot be set as finished' do
      @task = create(:task, state: 'anything')
      subject = described_class.new(record: @task, user: @task.owner)

      expect { subject.change_state }
        .to_not change { @task.reload.state }
        .from('anything')
    end
  end
end
