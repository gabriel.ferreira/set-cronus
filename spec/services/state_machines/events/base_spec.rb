require 'rails_helper'

class SampleEvent < StateMachines::Events::Base
	def new_state
		'new_state'
	end

	def allowed_original_states
		['allowed']
	end
end

describe StateMachines::Events::Base do
	describe '#change_state' do
		it 'changes state if included in #allowed_original_states' do
			task = build(:task, state: 'allowed')
      SampleEvent.new(record: task, user: task.owner).change_state
			expect(task.state).to eq 'new_state'
		end

		it 'do not changes states if not included in #allowed_original_states' do
			task = build(:task, state: 'not_allowed')
      SampleEvent.new(record: task, user: task.owner).change_state
			expect(task.state).to_not eq 'new_state'
		end

		context 'when given callbacks' do
			before do
				@callback_1 = double('callback_class')
				@callback_instance_1 = double('callback_instance', run: true)

				@callback_2 = double('callback_class')
				@callback_instance_2 = double('callback_instance', run: false)
			end

			it 'runs each callback if state allowed' do
				task = build(:task, state: 'allowed')

				expect(@callback_1)
          .to receive(:new)
          .with(record: task, user: task.owner, original_state: 'allowed', new_state: 'new_state')
          .and_return @callback_instance_1

				expect(@callback_2)
          .to receive(:new)
          .with(record: task, user: task.owner, original_state: 'allowed', new_state: 'new_state')
          .and_return @callback_instance_2

				expect(@callback_instance_1).to receive(:run).once
				expect(@callback_instance_2).to receive(:run).once

				SampleEvent.new(record: task,
                        user: task.owner,
                        callbacks: [@callback_1, @callback_2]).change_state
			end

			it 'do not runs callbacks if state not allowed' do
				task = build(:task, state: 'not_allowed')

				expect(@callback_1).to_not receive(:new)
				expect(@callback_2).to_not receive(:new)

				SampleEvent.new(record: task,
                        user: task.owner,
                        callbacks: [@callback_1, @callback_2]).change_state
			end
		end

    it 'records a new report' do
      task = build(:task, state: 'allowed')
      expect do
        SampleEvent.new(record: task, user: task.owner).change_state
      end.to change { TaskReport.count }.by 1

      report = TaskReport.last

      expect(report.original_state).to eq 'allowed'
      expect(report.project).to eq task.project
      expect(report.task).to eq task
      expect(report.user).to eq task.owner
      expect(report.new_state).to eq 'new_state'
    end

    it 'generates a new notification' do
      task = build(:task, state: 'allowed')
      expect do
        SampleEvent.new(record: task, user: task.owner).change_state
      end.to change { Notification.count }.by 1

      report = Notification.last

      expect(report.description).to be_a_kind_of String
      expect(report.seen?).to eq false
      expect(report.task).to eq task
      expect(report.user).to eq task.owner
      expect(report.project).to eq task.project
      expect(report.notification_type).to eq 'status_changed'
    end
  end
end
