# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Start do
  subject { described_class.new record: task, user: task.owner }

  context 'when task is ready' do
    let(:task) { create(:task, state: 'ready') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('ready')
        .to('started')
    end
  end

  context 'when task is anything else' do
    let(:task) { create(:task, state: 'anything') }

    it 'cannot be set as ready' do
      expect { subject.change_state }
        .to_not change { task.reload.state }
        .from('anything')
    end
  end
end
