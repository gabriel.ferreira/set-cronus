# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Reject do

  context 'when project is multi-user' do
    before do
      @task = create(:task)
      @task.project.contributors << create(:user)
    end

    it 'when state is verifying can be set as started' do
      @task.update_attributes(state: 'verifying')
      @subject = described_class.new record: @task, user: @task.owner

      expect { @subject.change_state }
        .to change { @task.state }
        .from('verifying')
        .to('started')
    end

    it 'when state is anytinhg else cannot be changed' do
      @task.update_attributes(state: 'anything')
      @subject = described_class.new record: @task, user: @task.owner

      expect { @subject.change_state }
        .to_not change { @task.state }
        .from('anything')
    end
  end

  context 'when project is single-user' do
    it 'when state is verifying cannot be changed' do
      @task = create(:task, state: 'verifying')
      @subject = described_class.new record: @task, user: @task.owner

      expect { @subject.change_state }
        .to_not change { @task.state }
        .from('verifying')
    end

    it 'when state is anytinhg else cannot be changed' do
      @task = create(:task, state: 'anything')
      @subject = described_class.new record: @task, user: @task.owner

      expect { @subject.change_state }
        .to_not change { @task.state }
        .from('anything')
    end
  end
end
