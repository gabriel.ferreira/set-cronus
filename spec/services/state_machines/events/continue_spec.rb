# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Continue do
  subject { described_class.new record: task, user: task.owner }

  context 'when task is stopped' do
    let(:task) { create(:task, state: 'stopped') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('stopped')
        .to('started')
    end
  end

  context 'when task is anything else' do
    let(:task) { create(:task, state: 'anything') }

    it 'cannot be changed' do
      expect { subject.change_state }
        .to_not change { task.reload.state }
        .from('anything')
    end
  end
end
