# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Stop do
  subject { described_class.new record: task, user: task.owner }

  context 'when task is started' do
    let(:task) { create(:task, state: 'started') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('started')
        .to('stopped')
    end
  end

  context 'when task is anything else' do
    let(:task) { create(:task, state: 'anything') }

    it 'cannot be set as ready' do
      expect { subject.change_state }
        .to_not change { task.reload.state }
        .from('anything')
    end
  end
end
