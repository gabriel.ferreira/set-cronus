require 'rails_helper'

describe StateMachines::Events::Discard do
  subject { described_class.new record: task, user: task.owner }

  context 'when task is scheduled' do
    let(:task) { create(:task, state: 'scheduled') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('scheduled')
        .to('discarded')
    end
  end

  context 'when task is verifying' do
    let(:task) { create(:task, state: 'verifying') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('verifying')
        .to('discarded')
    end
  end

  context 'when task is stopped' do
    let(:task) { create(:task, state: 'stopped') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('stopped')
        .to('discarded')
    end
  end

  context 'when task is ready' do
    let(:task) { create(:task, state: 'ready') }

    it 'can be set as started' do
      expect { subject.change_state }
        .to change { task.state }
        .from('ready')
        .to('discarded')
    end
  end

  context 'when task is anything else' do
    let(:task) { create(:task, state: 'anything') }

    it 'cannot be set as ready' do
      expect { subject.change_state }
        .to_not change { task.reload.state }
        .from('anything')
    end
  end
end
