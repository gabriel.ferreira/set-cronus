# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Finish do
  subject { described_class.new task }

  context 'when project is single-user' do
    it 'when task is started it changes to finished' do
      @task = create(:task, state: 'started')
      @subject = described_class.new(record: @task, user: @task.owner)

      expect { @subject.change_state }
        .to change { @task.reload.state }
        .from('started')
        .to('finished')
    end

    it 'when anything else it cannot be set as finished' do
      @task = create(:task, state: 'anything')
      @subject = described_class.new(record: @task, user: @task.owner)

      expect { @subject.change_state }
        .to_not change { @task.reload.state }
        .from('anything')
    end
  end

  context 'when project is multi-user' do
    before do
      @task = create(:task)
      @task.project.contributors << create(:user)
    end

    it 'when task is started it changes to finished' do
      @task.update_attributes(state: 'started')
      @subject = described_class.new(record: @task, user: @task.owner)

      expect { @subject.change_state }
        .to_not change { @task.reload.state }
        .from('started')
    end

    it 'when anything else it cannot be set as finished' do
      @task.update_attributes(state: 'anything')
      @subject = described_class.new(record: @task, user: @task.owner)

      expect { @subject.change_state }
        .to_not change { @task.reload.state }
        .from('anything')
    end
  end
end
