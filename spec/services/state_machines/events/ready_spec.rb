# frozen_string_literal: true
require 'rails_helper'

describe StateMachines::Events::Ready do
  subject { described_class.new record: task, user: task.owner }

  describe '#change_state' do
    context 'when the task is scheduled' do
      let(:task) { create(:task, state: 'scheduled') }

      context 'and when there are no dependencies' do
        it 'updates task to ready state' do
          expect { subject.change_state }
            .to change { task.state }
            .from('scheduled')
            .to('ready')
        end
      end

      context 'and when there are dependencies' do
        it 'and when dependency is scheduled it does not update task' do
          task.dependencies << create(:task, state: 'scheduled', project: task.project)

          expect { subject.change_state }
            .to_not change { task.state }
            .from('scheduled')
        end

        it 'and when dependency is ready it does not update task' do
          task.dependencies << create(:task, state: 'ready', project: task.project)

          expect { subject.change_state }
            .to_not change { task.state }
            .from('scheduled')
        end

        it 'and when dependency is finished updates the task to ready' do
          task.dependencies << create(:task, state: 'finished', project: task.project)

          expect { subject.change_state }
            .to change { task.state }
            .from('scheduled')
            .to('ready')
        end
      end
    end

    context 'when task is discarded' do
      let(:task) { create(:task, state: 'discarded') }

      context 'and when there are no dependencies' do
        it 'updates task to ready state' do
          expect { subject.change_state }
            .to change { task.state }
            .from('discarded')
            .to('ready')
        end
      end

      context 'and when there are dependencies' do
        it 'and when dependency is not finished it does not update task' do
          task.dependencies << create(:task, state: 'scheduled', project: task.project)

          expect { subject.change_state }
            .to_not change { task.state }
            .from('discarded')
        end

        it 'and when dependency is finished updates the task to ready' do
          task.dependencies << create(:task, state: 'finished', project: task.project)

          expect { subject.change_state }
            .to change { task.state }
            .from('discarded')
            .to('ready')
        end
      end
    end

    context 'when task is not scheduled' do
      let(:task) { create(:task, state: 'anything') }

      it 'do not updates task' do
        expect { subject.change_state }
          .to_not change { task.state }
          .from('anything')
      end
    end
  end
end
