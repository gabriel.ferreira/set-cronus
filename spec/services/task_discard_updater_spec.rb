require 'rails_helper'

describe TaskDiscardUpdater do
	describe '#update' do
		before do
			@task = create(:task, state: 'ready')
      @contributor = create(:user)
			@task.project.contributors << @contributor
		end

		it 'sets given task to discarded' do
      described_class.new(@task, @task.owner).update
			expect(@task.reload.state).to eq 'discarded'
		end

    it 'removes any contributor from the task' do
      @task.contributors << @contributor

      described_class.new(@task, @task.owner).update
			expect(@task.reload.contributors).to be_empty
    end

    it 'removes all kinds of linkings with other tasks' do
      @dependency = create(:task, project: @task.project)
      @dependent_task = create(:task, project: @task.project)
      @task.dependencies << @dependency
      @dependent_task.dependencies << @task
      @dependent_task.dependencies << @dependency

      described_class.new(@task, @task.owner).update

			expect(@task.reload.dependencies).to be_empty
			expect(@task.reload.requisites).to be_empty

			expect(@dependency.reload.requisites)
        .to contain_exactly @dependent_task

			expect(@dependent_task.reload.dependencies)
        .to contain_exactly @dependency
    end

    it 'sets dependent tasks to ready or started' do
      @dependency_1 = create(:task, project: @task.project, state: 'scheduled')
      @dependency_2 = create(:task, project: @task.project, state: 'stopped')
      @dependency_3 = create(:task, project: @task.project, state: 'scheduled')
      @dependency_4 = create(:task, project: @task.project, state: 'stopped')

      @dependency_3.dependencies << create(:task)
      @dependency_4.dependencies << create(:task)

      @task.dependencies << [@dependency_1, @dependency_2]

      described_class.new(@task, @task.owner).update

      expect(@dependency_1.reload.state).to eq 'ready'
      expect(@dependency_2.reload.state).to eq 'started'
      expect(@dependency_3.reload.state).to eq 'scheduled'
      expect(@dependency_4.reload.state).to eq 'stopped'
    end
	end
end
