require 'rails_helper'

describe TaskAcceptUpdater do
	describe '#update' do
		before do
			@task = create(:task, state: 'verifying')
			@task.project.contributors << create(:user)
		end

		it 'sets given task to finished' do
      described_class.new(@task, @task.owner).update

			expect(@task.reload.state).to eq 'finished'
		end

		it 'updates dependencies ready to start' do
			expect_any_instance_of(DependencyReadyUpdater)
			  .to receive(:update)

      described_class.new(@task, @task.owner).update
		end
	end
end
