# frozen_string_literal: true
require 'rails_helper'

describe DependencyFinder do
  let(:task) { build(:task) }
  let(:first_level_dependencies) { build_list(:task, 2, project: task.project) }
  let(:second_level_dependencies) { build_list(:task, 4, project: task.project) }
  let(:third_level_dependencies) { build_list(:task, 3, project: task.project) }

  subject { described_class.new(task) }

  describe '#find' do
    before do
      task.dependencies << first_level_dependencies
      task.dependencies[0].dependencies << second_level_dependencies
      task.dependencies[1].dependencies << second_level_dependencies[0]
      task.dependencies[0].dependencies << third_level_dependencies
    end

    it 'returns all direct and indirect dependencies' do
      dependencies_array = first_level_dependencies +
                           second_level_dependencies +
                           third_level_dependencies

      dependencies_found = subject.find

      expect(dependencies_found)
        .to match_array(dependencies_array)
    end
  end
end
