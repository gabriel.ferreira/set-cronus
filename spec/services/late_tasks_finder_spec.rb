require 'rails_helper'

describe LateTasksFinder do
  describe '#call' do
    before do
      @not_late_tasks = [
        create(:task, ending_date: 2.days.from_now),
        create(:task, ending_date: 1.day.from_now),
        create(:task, ending_date: Time.zone.today.end_of_day),
        create(:task, ending_date: Time.zone.today.beginning_of_day)
      ]

      @late_tasks = [
        create(:task, ending_date: 2.days.ago),
        create(:task, ending_date: 1.days.ago),
        create(:task, ending_date: Time.zone.today.beginning_of_day - 1.hour),
      ]
    end

    it 'selects only late tasks' do
      expect(described_class.new.find)
        .to contain_exactly *@late_tasks
    end

    it 'does not select not late tasks' do
      tasks = described_class.new.find

      tasks.each do |task|
        expect(@not_late_tasks).to_not include task
      end
    end

    it 'ignores finished tasks' do
      @finished_task = create(:task,
                            ending_date: 2.days.ago,
                            state: 'finished')

      expect(described_class.new.find)
        .to_not include @finished_task
    end

    it 'ignores discarded tasks' do
      @finished_task = create(:task,
                            ending_date: 2.days.ago,
                            state: 'discarded')

      expect(described_class.new.find)
        .to_not include @finished_task
    end
  end
end
