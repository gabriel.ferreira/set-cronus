# frozen_string_literal: true
require 'rails_helper'

describe OwnershipAuthenticator do
  let(:resource) { double('authenticable', owner: owner) }
  let(:owner) { create(:user) }

  subject { described_class.new(resource: resource, user: current_user) }

  describe 'owner?' do
    context 'when current_user has the ownership' do
      let(:current_user) { owner }

      it 'returns true' do
        expect(subject.owner?).to be true
      end
    end

    context 'when current_user have not the ownership' do
      let(:current_user) { create(:user) }

      it 'returns true' do
        expect(subject.owner?).to be false
      end
    end

    context 'when user is not authenticated properly' do
      let(:current_user) { nil }

      it 'returns false and does not raise error' do
        expect(subject.owner?).to be false
      end
    end
  end

  describe 'allowed_in?' do
    context 'when user is resource owner' do
      let(:current_user) { owner }

      it 'returns true' do
        expect(subject.allowed_in?).to be true
      end
    end

    context 'when user is a contributor' do
      let(:current_user) { create(:user) }

      before do
        allow(resource)
          .to receive(:contributors)
          .and_return [current_user]
      end

      it 'returns true' do
        expect(subject.allowed_in?).to be true
      end
    end

    context 'when user is foreign user' do
      let(:current_user) { create(:user) }

      before do
        allow(resource)
          .to receive(:contributors)
          .and_return([])
      end

      it 'returns false' do
        expect(subject.allowed_in?).to be false
      end
    end

    context 'when user is not authenticated properly' do
      let(:current_user) { nil }

      it 'returns false and does not raise error' do
        expect(subject.allowed_in?).to be false
      end
    end
  end
end
