# frozen_string_literal: true
require 'rails_helper'

describe ProjectUpdater do
  let!(:now) { Time.zone.now + 1.hour }
  let(:project) { create(:project, starting_date: now) }

  let(:update_params) do
    { name: 'foo',
      starting_date: now + 10.days,
      ending_date: now + 15.days }
  end

  subject { described_class.new project }

  describe '#update!' do
    context 'when project does not have tasks' do
      it "updates the project's attrs" do
        subject.update(update_params)

        update_params.each_pair do |attr, value|
          value_got = project.reload.send(attr.to_sym).to_s

          expect(value_got).to eq value.to_s
        end
      end
    end

    context 'when the project has tasks' do
      let!(:tasks) do
        [create_task(project.starting_date, now + 5.days),
         create_task(now + 10.days, now + 11.days),
         create_task(now + 15.days, now + 18.days)]
      end

      it 'updates older tasks with the new starting date' do
        subject.update(update_params)

        expect(tasks[0].reload.starting_date.to_s).to eq (now + 10.days).to_s
        expect(tasks[0].reload.ending_date.to_s).to eq (now + 15.days).to_s

        expect(tasks[1].reload.starting_date.to_s).to eq (now + 10.days).to_s
        expect(tasks[1].reload.ending_date.to_s).to eq (now + 11.days).to_s

        expect(tasks[2].reload.starting_date.to_s).to eq (now + 15.days).to_s
        expect(tasks[2].reload.ending_date.to_s).to eq (now + 18.days).to_s
      end
    end

    def create_task(starting_date, ending_date)
      create(:task, project: project,
                    starting_date: starting_date,
                    ending_date: ending_date)
    end
  end
end
