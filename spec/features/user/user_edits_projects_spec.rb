# frozen_string_literal: true
require 'rails_helper'

feature 'user edits projects' do
  before do
    @project = create(:project)
    login_as @project.user
    visit project_path(@project)

    click_on 'edit_project'
  end

  scenario 'successfully' do
    fill_in_form(name: 'new_name',
                 starting_date: Time.zone.now + 20.days,
                 ending_date: Time.zone.now + 21.days)

    expect(page).to have_css 'h1', text: 'new_name'
    expect(page).to have_content '21 days remaining'
    expect(page).to have_content "Starting at #{I18n.l Time.zone.now + 20.days, format: :long}"
  end

  scenario 'with empty name' do
    expect do
      fill_in_form(name: '',
                   starting_date: Time.zone.now + 20.days,
                   ending_date: Time.zone.now + 21.days)
    end.to_not change { @project.reload }

    expect(page).to have_content "Name can't be blank"
  end

  scenario 'with starting date before today' do
    expect do
      fill_in_form(name: 'new_name',
                   starting_date: 1.day.ago,
                   ending_date: Time.zone.now + 21.days)
    end.to_not change { @project.reload }
  end

  scenario 'with ending date before today' do
    expect do
      fill_in_form(name: 'new_name',
                   starting_date: 10.days.ago,
                   ending_date: 1.day.ago)
    end.to_not change { @project.reload }

    expect(page).to have_content 'The ending date must be after today'
  end

  scenario 'with ending date lower than starting date' do
    expect do
      fill_in_form(name: 'new_name',
                   starting_date: 5.days.from_now,
                   ending_date: 3.days.from_now)
    end.to_not change { @project.reload }

    expect(page).to have_content ''
  end

  scenario 'with empty starting date' do
    expect do
      fill_in_form(name: 'new_name',
                   starting_date: nil,
                   ending_date: 1.day.from_now)
    end.to_not change { @project.reload }

    expect(page).to have_content 'Starting date is missing'
  end

  scenario 'with empty ending date' do
    expect do
      fill_in_form(name: 'new_name',
                   starting_date: 1.day.from_now,
                   ending_date: nil)
    end.to_not change { @project.reload }

    expect(page).to have_content 'Ending date is missing'
  end

  context 'when project is starting today' do
    scenario 'and changes starting date' do
      today =  Time.zone.now
      @project.update_attributes(starting_date: today)

      expect do
        fill_in_form(name: 'new_name',
                     starting_date: @project.starting_date + 1.day,
                     ending_date: @project.ending_date)

      end.to change { @project.reload.starting_date.to_i }
    end

    scenario 'and changes ending date' do
      @project.update_attributes(starting_date: Time.zone.now)

      expect do
        fill_in_form(name: 'new_name',
                     starting_date: @project.starting_date,
                     ending_date: @project.ending_date + 1.day)
      end.to change { @project.reload.ending_date.to_i }
    end
  end

  context 'when project is ending today' do
    scenario 'and changes starting date' do
      today =  Time.zone.now
      @project.update_attributes(ending_date: today)

      expect do
        fill_in_form(name: 'new_name',
                     starting_date: @project.starting_date + 1.day,
                     ending_date: @project.ending_date)

      end.to_not change { @project.reload.starting_date.to_i }
    end

    scenario 'and changes ending date' do
      @project.update_attributes(ending_date: Time.zone.now)

      expect do
        fill_in_form(name: 'new_name',
                     starting_date: @project.starting_date,
                     ending_date: @project.ending_date + 1.day)
      end.to change { @project.reload.ending_date.to_i }
    end
  end

  def fill_in_form(name:, starting_date:, ending_date:)
    fill_in 'project_starting_date', with: starting_date
    fill_in 'project_ending_date', with: ending_date
    fill_in 'project_name', with: name

    click_on 'submit'
  end
end
