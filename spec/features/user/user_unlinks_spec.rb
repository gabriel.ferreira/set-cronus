# frozen_string_literal: true
require 'rails_helper'

feature 'user unlinks tasks' do
  before do
    @project = create(:project)
    @tasks = create_list(:task, 3, project: @project)

    @tasks[0].dependencies << @tasks[1]
    @tasks[1].dependencies << @tasks[2]

    login_as @project.user

    visit project_tasks_path(@project)

    within "#task-#{@tasks[1].id}" do
      click_on 'unlink_task'
    end
  end

  scenario 'successfully' do
    select @tasks[2].name, from: 'dependency'
    click_on 'submit'

    expect(@tasks[0].reload.dependencies).to include @tasks[1]
    expect(@tasks[1].reload.dependencies).to be_empty
  end

  scenario 'and task becomes ready' do
    @tasks[0..1].each { |t| t.update_attributes(state: 'scheduled') }

    select @tasks[2].name, from: 'dependency'
    click_on 'submit'

    expect(@tasks[0].reload.state).to eq 'scheduled'
    expect(@tasks[1].reload.state).to eq 'ready'
  end

  scenario 'an task with more dependencies still as scheduled' do
    @tasks[0..1].each { |t| t.update_attributes(state: 'scheduled') }

    @tasks[1].dependencies << create(:task, project: @project)

    select @tasks[2].name, from: 'dependency'
    click_on 'submit'

    expect(@tasks[0].reload.state).to eq 'scheduled'
    expect(@tasks[1].reload.state).to eq 'scheduled'
  end
end
