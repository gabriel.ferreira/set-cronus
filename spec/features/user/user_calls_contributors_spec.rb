# frozen_string_literal: true
require 'rails_helper'

feature 'user calls contributors' do
  scenario 'successfully' do
    owner = create(:user)
    contributor = create(:user)

    login_as owner

    project = create(:project, user: owner)

    visit project_path(project)

    click_on 'new_contributor'

    fill_in 'contributor_name', with: contributor.name
    click_on 'submit'

    expect(page).to have_css('h1', text: 'Contributors')
    expect(page).to have_css('.contributor', text: contributor.name)

    expect(project.reload.contributors)
      .to include(contributor)
  end

  scenario 'with a unexistent user' do
    owner = create(:user)
    login_as owner

    project = create(:project, user: owner)

    visit new_project_contributor_path(project)
    click_on 'submit'

    expect(page).to have_content('Could not find this contributor')
    expect(page).to have_css '#submit'
  end
end
