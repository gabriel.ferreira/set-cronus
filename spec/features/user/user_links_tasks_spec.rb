# frozen_string_literal: true
require 'rails_helper'

feature 'user links tasks' do
  scenario 'successfully' do
    project = create(:project)
    login_as project.user

    tasks = create_list(:task, 2, project: project)

    visit project_path(project)

    click_on 'link_tasks'

    select tasks[0].name, from: 'task_requisite'
    select tasks[1].name, from: 'task_dependency'

    click_on 'submit'

    expect(tasks[0].reload.dependencies)
      .to include tasks[1]

    expect(tasks[1].reload.requisites)
      .to include tasks[0]
  end
end
