# frozen_string_literal: true
require 'rails_helper'

feature 'user removes contributor' do
  scenario 'successfully' do
    user = create(:user)
    contributors = create_list(:user, 2)
    user_project = create(:project, user: user)

    user_project.contributors << contributors

    login_as user
    visit project_contributors_path(user_project)
    within "#contributor-#{contributors[0].id}" do
      click_on 'delete_contributor'
    end

    expect(page).to_not have_content contributors[0].name
    expect(page).to_not have_content contributors[0].email

    expect(page).to have_content contributors[1].name
    expect(page).to have_content contributors[1].email
  end
end
