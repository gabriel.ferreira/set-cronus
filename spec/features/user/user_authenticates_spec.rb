# frozen_string_literal: true
require 'rails_helper'

feature 'user authentication' do
  scenario 'user signs up successfully' do
    name = 'someone'
    email = 'someone@example.com'
    password = 'password'

    visit root_path

    click_on 'user_sign_up'

    fill_in 'user_email', with: email
    fill_in 'user_name', with: name
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password

    click_on 'submit'

    expect(User.count).to eq 1

    new_created_user = User.last
    expect(new_created_user.name).to eq name
    expect(new_created_user.email).to eq email
  end

  scenario 'user signs in successfully' do
    user = create(:user)

    visit root_path

    click_on 'user_sign_in'

    fill_in 'user_email', with: user.email
    fill_in 'user_password', with: user.password

    click_on 'submit'

    expect(page).to have_content user.name
  end

  scenario 'user signs out successfully' do
    user = create(:user)
    login_as user
    visit user_path(user)

    click_on 'user_sign_out'

    expect(page).to_not have_content(user.name)
    expect(page).to have_selector(:link_or_button, 'user_sign_in')
    expect(page).to have_selector(:link_or_button, 'user_sign_up')
    expect(page).to_not have_selector(:link_or_button, 'user_sign_out')
  end
end
