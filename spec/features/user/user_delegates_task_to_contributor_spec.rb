# frozen_string_literal: true
require 'rails_helper'

feature 'user delegates task to contributor' do
  let!(:owner) { create(:user) }
  let!(:contributor) { create(:user) }
  let!(:project) { create(:project, user: owner) }
  let!(:task) { create(:task, project: project) }

  before do
    login_as owner

    project.contributors << contributor
  end

  scenario 'successfully' do
    visit project_tasks_path(project)

    within "#task-#{task.id}" do
      click_on 'new_tasks_assignee'
    end

    select contributor.name, from: 'assignee'
    click_on 'submit'

    within "#task-#{task.id}" do
      expect(page).to have_css '.contributors_count', text: 1
      expect(task.reload.contributors.count).to eq 1
    end
  end

  scenario 'removes project owner from task contributors' do
    task.contributors << owner

    visit new_task_assignee_path(task)
    select contributor.name, from: 'assignee'
    click_on 'submit'

    expect(task.reload.contributors)
      .to eq [contributor]
  end
end
