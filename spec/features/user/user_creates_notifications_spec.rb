require 'rails_helper'

feature 'user creates notifications' do
  scenario 'by changing task state' do
    @project = create(:project)
    @task = create(:task, project: @project, state: 'ready')
    @task.contributors << @task.owner

    login_as @project.owner

    visit task_path(@task)

    expect do
      click_on 'task_start'
    end.to change { Notification.count }.by 1
  end

  scenario 'by finishing a task' do
    @project = create(:project)
    @task = create(:task, project: @project, state: 'started')
    @task.contributors << @task.owner

    login_as @project.owner

    visit task_path(@task)

    expect do
      click_on 'task_finish'
    end.to change { Notification.count }.by 2
  end
end
