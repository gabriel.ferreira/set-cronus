# frozen_string_literal: true
require 'rails_helper'

feature 'user visits root path' do
  scenario 'successfully' do
    visit root_path

    expect(page.body).to match /SetCronus/
  end
end
