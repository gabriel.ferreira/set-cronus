# frozen_string_literal: true
require 'rails_helper'

feature 'user edits tasks' do

  before do
    @project = create(:project, starting_date: Time.zone.yesterday)
    @task = create(:task, project: @project)

    login_as @project.user

    visit project_tasks_path(@project)

    click_on 'edit_task'
  end

  scenario 'succesfully' do
    fill_in_form(name: 'new name',
                 description: 'new description',
                 starting_date: Time.zone.now + 10.days,
                 ending_date: Time.zone.now + 11.days)

    expect(page).to have_content 'new name'
    expect(page).to have_content 'new description'
    expect(page).to have_content '11 days remaining'
    expect(page).to have_content "Starting at #{I18n.l Time.zone.now + 10.days, format: :long}"
  end

  scenario 'with blank name' do
    expect do
      fill_in_form(name: nil,
                   description: 'new description',
                   starting_date: Time.zone.now + 10.days,
                   ending_date: Time.zone.now + 11.days)
    end.to_not change { @task.reload }

    expect(page).to have_content "Name can't be blank"
  end

  scenario 'with blank starting date' do
    expect do
      fill_in_form(name: 'new name',
                   description: 'new description',
                   starting_date: nil,
                   ending_date: Time.zone.now + 11.days)
    end.to_not change { @task.reload }

    expect(page).to have_content "Starting date is missing"
  end

  scenario 'with blank ending date' do
    expect do
      fill_in_form(name: 'new name',
                   description: 'new description',
                   starting_date: Time.zone.now + 10.days,
                   ending_date: nil)
    end.to_not change { @task.reload }

    expect(page).to have_content "Ending date is missing"
  end

  scenario 'with starting date after ending date' do
    starting_date = @project.starting_date + 1.day

    expect do
      fill_in_form(name: @task.name,
                   description: @task.description,
                   starting_date: starting_date,
                   ending_date: starting_date - 1.hour)
    end.to_not change { @task.reload }

    expect(page).to have_content 'Task cannot finish before has started'
  end

  scenario 'with starting date before today' do
    expect do
      fill_in_form(name: @task.name,
                   description: @task.description,
                   starting_date: Time.zone.today.beginning_of_day - 1.hour,
                   ending_date: @project.ending_date)
    end.to change { @task.reload.starting_date }
  end

  scenario 'with starting date before project starting date' do
    expect do
      fill_in_form(name: @task.name,
                   description: @task.description,
                   starting_date: @project.starting_date - 1.hour,
                   ending_date: @project.ending_date)
    end.to_not change { @task.reload.starting_date.to_i }

    expect(page).to have_content 'The task cannot start before the project'
  end

  scenario 'with ending date after project ending date' do
    expect do
      fill_in_form(name: @task.name,
                   description: @task.description,
                   starting_date: @task.starting_date,
                   ending_date: @project.ending_date + 1.day)
    end.to change { @task.reload.ending_date.to_i }
  end

  def fill_in_form(name:, description:, starting_date:, ending_date:)
    fill_in 'task_name', with: name
    fill_in 'task_description', with: description
    fill_in 'task_starting_date', with: starting_date
    fill_in 'task_ending_date', with: ending_date

    click_on 'submit'
  end
end
