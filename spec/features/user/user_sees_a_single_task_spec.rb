# frozen_string_literal: true
require 'rails_helper'

feature 'user sees a single task' do
  scenario 'successfully' do
    task = create(:task)

    login_as task.owner

    visit project_tasks_path(task.project)

    click_on task.name

    expect(page).to have_content(task.name)
    expect(page).to have_content(task.description)
  end
end
