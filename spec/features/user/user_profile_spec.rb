# frozen_string_literal: true
require 'rails_helper'

feature 'user profile' do
  scenario 'non authorized user cannot access other profiles' do
    user = create(:user)
    clandestine = create(:user, name: 'clandestine')

    login_as clandestine

    visit user_path(user)

    expect(page).to have_content clandestine.name
    expect(page).to_not have_content user.name
  end

  scenario 'non authenticated user cannot access profiles' do
    user = create(:user)

    visit user_path(user)

    expect(current_path).to eq new_user_session_path
  end
end
