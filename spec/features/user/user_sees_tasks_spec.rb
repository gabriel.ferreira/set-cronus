# frozen_string_literal: true
require 'rails_helper'

feature 'user sees tasks' do
  let!(:project) { create(:project) }
  let!(:tasks) { create_list(:task, 3, project: project) }

  before do
    login_as project.user
  end

  scenario 'successfully' do
    visit project_tasks_path(project)

    expect(page).to have_content tasks[0].name
    expect(page).to have_content tasks[0].state

    expect(page).to have_content tasks[1].name
    expect(page).to have_content tasks[1].state

    expect(page).to have_content tasks[2].name
    expect(page).to have_content tasks[2].state
  end

  scenario 'by clicking in a link at project page' do
    visit project_path(project)

    click_on 'project_tasks'

    expect(page).to have_content tasks[0].name
    expect(page).to have_content tasks[0].state

    expect(page).to have_content tasks[1].name
    expect(page).to have_content tasks[1].state

    expect(page).to have_content tasks[2].name
    expect(page).to have_content tasks[2].state
  end

  scenario 'only by accessing tasks index page' do
    visit project_path(project)

    expect(page).to_not have_content tasks[0].name
    expect(page).to_not have_content tasks[1].name
    expect(page).to_not have_content tasks[2].name
  end
end
