# frozen_string_literal: true
require 'rails_helper'

feature 'user creates project' do
  before do
    user = create(:user)
    login_as user

    visit user_path(user)

    click_on 'new_project'
  end

  scenario 'successfully' do
    fill_in_data(name: 'project name',
                 starting_date: Time.zone.now + 5.days,
                 ending_date: Time.zone.now + 20.days)

    expect(page).to have_css 'h1', text: 'project name'
    expect(page).to have_content '20 days remaining'
    expect(page).to have_content 'Starting at'
  end

  scenario 'starting before today' do
    expect do
      fill_in_data(name: 'project name',
                 starting_date: 1.day.ago,
                 ending_date: Time.zone.now + 20.days)
    end.to_not change { Project.count }

    expect(page).to have_content "The starting date must be after today"
  end

  scenario 'starting today' do
    expect do
      fill_in_data(name: 'project name',
                   starting_date: Time.zone.now,
                 ending_date: Time.zone.now + 20.days)
    end.to change { Project.count }

    expect(page).to have_css 'h1', text: 'project name'
  end

  scenario 'ending before starting date' do
    expect do
      fill_in_data(name: 'project name',
                   starting_date: Time.zone.now + 1.day,
                 ending_date: Time.zone.now)
    end.to_not change { Project.count }

    expect(page).to have_content "Project cannot finish before has started"
  end

  scenario 'with empty name' do
    expect do
      fill_in_data(name: nil,
                   starting_date: Time.zone.now,
                 ending_date: Time.zone.now + 1.day)
    end.to_not change { Project.count }
  end

  def fill_in_data(starting_date:, ending_date:, name:)
    fill_in 'project_name', with: name
    fill_in 'project_ending_date', with: ending_date
    fill_in 'project_starting_date', with: starting_date

    click_on 'submit'
  end
end
