require 'rails_helper'

feature 'user sees notifications' do
  before do
    @user = create(:user)
    @user_notification = create(:notification, user: @user)
    @someone_else_notification = create(:notification)

    login_as @user
  end

  scenario 'at navigation bar as small label when has new ones' do
    visit user_path(@user)

    expect(page).to have_css('#notifications')
    expect(page).to have_css('#notifications_count', text: '1')

    click_on('notifications')

    expect(page).to have_content @user_notification.description
    expect(page).to_not have_content @someone_else_notification.description
  end

  scenario 'by clicking on navigations menu bar in project layout' do
    project = create(:project, user: @user)
    visit project_path(project)

    expect(page).to have_css('#notifications')
    expect(page).to have_css('#notifications_count', text: '1')

    click_on('notifications')

    expect(page).to have_content @user_notification.description
    expect(page).to_not have_content @someone_else_notification.description
  end

  scenario 'by clicking on navigations menu bar in task layout' do
    project = create(:project, user: @user)
    task = create(:task, project: project)
    visit task_path(task)

    expect(page).to have_css('#notifications')
    expect(page).to have_css('#notifications_count', text: '1')

    click_on('notifications')

    expect(page).to have_content @user_notification.description
    expect(page).to_not have_content @someone_else_notification.description
  end

  scenario 'by clicking on user profile menu' do

    #expect(page).to have_css('#notifications')
    #expect(page).to have_css('#notifications_count', text: '1')

    #click_on('notifications')

    #expect(page).to have_content @user_notification.description
    #expect(page).to_not have_content @someone_else_notification.description
  end

  scenario 'and dismiss notification' do
    visit user_path(@user)
    click_on('notifications')

    expect(page).to have_content @user_notification.description
    expect(@user_notification.reload.seen?).to be false
    click_on('dismiss')

    expect(page).to_not have_content @user_notification.description
    expect(@user_notification.reload.seen?).to be true
  end
end
