require 'rails_helper'

feature 'user removes project' do
  before do
    @project = create(:project)
    login_as @project.user

    visit user_projects_path(@project.user)
  end

  scenario 'to the trash successfully' do
    move_project_to_trash(@project)

    expect(@project.reload.in_trash?).to be true
    success_message = "Moved project #{@project.name} to the trash"
    expect(page).to have_content success_message

    visit user_trashes_path(@project.user)

    within "#project-#{@project.id}" do
      expect(page).to have_content @project.name
      expect(page).to have_button "Restore"
      expect(page).to have_button "Delete"
    end
  end

  scenario 'from another user' do
    another_user = create(:user)
    other_project = create(:project, user: another_user)

    login_as another_user

    visit user_trashes_path(@project.user)

    expect(page).to_not have_content(@project.name)
  end

  scenario 'to the trash by accessing project panel' do
    visit project_path(@project)

    click_on 'move_to_trash'

    success_message = "Moved project #{@project.name} to the trash"
    expect(page).to have_content success_message
    expect(@project.reload.in_trash?).to be true
  end

  scenario 'to the trash unsuccessfully' do
    allow_any_instance_of(Project)
      .to receive(:update_attributes)
      .and_return false

    move_project_to_trash(@project)

    expect(@project.reload.in_trash?).to be false
    error_message = "Could not move project #{@project.name} to the trash"
    expect(page).to have_content error_message

    visit user_trashes_path(@project.user)
    expect(page).to_not have_content(@project.name)
  end

  scenario 'permanently' do
    @project.update_attributes(in_trash: true)
    visit user_trashes_path(@project.user)

    click_on 'destroy_project'

    success_message = "Project #{@project.name} deleted successfully"
    expect(page).to have_content success_message

    visit user_trashes_path(@project.user)
    expect(page).to_not have_content(@project.name)

    visit user_projects_path(@project.user)
    expect(page).to_not have_content(@project.name)
  end

  scenario 'and then restores it successfully' do
    @project.update_attributes(in_trash: true)
    visit user_trashes_path(@project.user)

    click_on 'restore_project'

    success_message = "Moved project #{@project.name} out of the trash"
    expect(page).to have_content success_message

    visit user_trashes_path(@project.user)
    expect(page).to_not have_content(@project.name)

    visit user_projects_path(@project.user)
    expect(page).to have_content(@project.name)
  end

  scenario 'and then restores it unsuccessfully' do
    @project.update_attributes(in_trash: true)

    allow_any_instance_of(Project)
      .to receive(:update_attributes)
      .and_return false

    visit user_trashes_path(@project.user)

    click_on 'restore_project'

    error_message = "Could not move project #{@project.name} out of the trash"
    expect(page).to have_content error_message

    visit user_trashes_path(@project.user)
    expect(page).to have_content(@project.name)

    visit user_projects_path(@project.user)
    expect(page).to_not have_content(@project.name)
  end

  scenario 'and searches for trashed projects in index' do
    @project.update_attributes(in_trash: true)

    visit user_projects_path(@project.user)
    expect(page).to_not have_content(@project.name)
  end

  scenario 'and removes a non-empty project permanently' do
    @project.update_attributes(in_trash: true)
    @project.tasks << create(:task, project: @project)
    @project.contributors << create(:user)

    visit user_trashes_path(@project.user)

    click_on 'destroy_project'

    visit user_projects_path(@project.user)
    expect(page).to_not have_content(@project.name)

    visit user_trashes_path(@project.user)
    expect(page).to_not have_content(@project.name)
  end

  def move_project_to_trash(project)
    within "#project-#{project.id}" do
      click_on 'move_to_trash'
    end
  end
end
