# frozen_string_literal: true
require 'rails_helper'

feature 'user sees projects' do
  scenario 'successfully' do
    user = create(:user)
    projects = create_list(:project, 2, user: user)
    someone_else_projects = create(:project)

    login_as user
    visit user_path(user)

    expect(page).to_not have_content(projects[0].name)
    expect(page).to_not have_content(projects[1].name)
    expect(page).to_not have_content(someone_else_projects.name)

    click_on 'my_projects'

    expect(page).to have_content(projects[0].name)
    expect(page).to have_content(projects[1].name)
    expect(page).to_not have_content(someone_else_projects.name)
  end

  scenario 'when there is no project' do
    user = create(:user)
    login_as user
    visit user_projects_path(user)

    expect(page).to have_content('You do not have projects yet. Want to create a new one?')
    expect(page).to have_link('new_project')
  end

  scenario 'when there is any project' do
    user = create(:user)
    project = create(:project, user: user)

    login_as user
    visit user_projects_path(user)

    expect(page).to_not have_content('You do not have projects yet. Want to create a new one?')
    expect(page).to_not have_link('new_project')
  end
end
