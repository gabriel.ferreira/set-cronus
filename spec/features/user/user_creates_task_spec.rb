# frozen_string_literal: true
require 'rails_helper'

feature 'user creates a task' do
  let(:project) { create(:project) }
  let(:task) { build(:task, project: nil) }
  let(:new_starting_date) { project.starting_date + 1.day }

  before do
    login_as project.user
    visit project_path(project)

    click_on 'new_task'
  end

  scenario 'successfully' do
    fill_in_form(name: task.name,
                 description: task.description,
                 starting_date: new_starting_date,
                 ending_date: project.ending_date)

    expect(page).to have_content task.name
    expect(page).to have_content task.description
    expect(page).to have_content '10 days remaining'
    expect(page).to have_content "Starting at #{I18n.l new_starting_date, format: :long}"
  end

  scenario 'and sets himself as contributor' do
    fill_in_form(name: task.name,
                 description: task.description,
                 starting_date: new_starting_date,
                 ending_date: project.ending_date)

    expect(Task.last.contributors)
      .to include(project.user)
  end

  scenario 'without name' do
    expect do
      fill_in_form(name: nil,
                   description: task.description,
                   starting_date: new_starting_date,
                   ending_date: project.ending_date)
    end.to_not change { Task.count }

    expect(page).to have_content "Name can't be blank"
  end

  scenario 'without description' do
    expect do
      fill_in_form(name: task.name,
                   description: nil,
                   starting_date: new_starting_date,
                   ending_date: project.ending_date)
    end.to change { Task.count }
  end

  scenario 'without starting date' do
    expect do
      fill_in_form(name: task.name,
                   description: task.description,
                   starting_date: nil,
                   ending_date: project.ending_date)
    end.to_not change { Task.count }

    expect(page).to have_content "Starting date is missing"
  end

  scenario 'without ending date' do
    expect do
      fill_in_form(name: task.name,
                   description: task.description,
                   starting_date: new_starting_date,
                   ending_date: nil)
    end.to_not change { Task.count }

    expect(page).to have_content "Ending date is missing"
  end

  scenario 'with starting date after ending date' do
    starting_date = project.starting_date + 1.day

    expect do
      fill_in_form(name: task.name,
                   description: task.description,
                   starting_date: starting_date,
                   ending_date: starting_date - 1.hour)
    end.to_not change { Task.count }

    expect(page).to have_content 'Task cannot finish before has started'
  end

  scenario 'with starting date before today' do
    starting_date = Time.zone.today.beginning_of_day

    expect do
      fill_in_form(name: task.name,
                   description: task.description,
                   starting_date: starting_date - 1.day,
                   ending_date: project.ending_date)
    end.to_not change { Task.count }

    expect(page).to have_content 'The starting date must be after today'
  end

  scenario 'with starting date before project starting date' do
    expect do
      fill_in_form(name: task.name,
                   description: task.description,
                   starting_date: project.starting_date - 1.hour,
                   ending_date: project.ending_date)
    end.to_not change { Task.count }

    expect(page).to have_content 'The task cannot start before the project'
  end

  scenario 'with ending date after project ending date' do
    expect do
      fill_in_form(name: task.name,
                   description: task.description,
                   starting_date: new_starting_date,
                   ending_date: project.ending_date + 1.day)
    end.to change { Task.count }
  end

  def fill_in_form(name:, description:, starting_date:, ending_date:)
    fill_in 'task_name', with: name
    fill_in 'task_description', with: description
    fill_in 'task_starting_date', with: starting_date
    fill_in 'task_ending_date', with: ending_date

    click_on 'submit'
  end
end
