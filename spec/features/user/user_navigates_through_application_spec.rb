# frozen_string_literal: true
require 'rails_helper'

feature 'user navigates through application' do
  before do
    @user = create(:user)
    login_as @user

    visit user_path(@user)
  end

  scenario 'successfully' do
    within('nav') do
      expect(page).to have_css("a[href='#{user_path(@user)}']")
      expect(page).to have_css("a[href='#{user_projects_path(@user)}']")
      expect(page).to have_css("a[href='#{shared_projects_user_path(@user)}']")
      expect(page).to have_css("a[href='#{user_trashes_path(@user)}']")
    end
  end

  scenario 'from profile' do
    click_on 'my_projects'
    expect(current_path).to eq user_projects_path(@user)

    visit user_path(@user)
    click_on 'shared_projects'
    expect(current_path).to eq shared_projects_user_path(@user)

    visit user_path(@user)
    click_on 'trash'
    expect(current_path).to eq user_trashes_path(@user)
  end
end
