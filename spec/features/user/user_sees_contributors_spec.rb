# frozen_string_literal: true
require 'rails_helper'

feature 'user sees contributors' do
  scenario 'successfully' do
    project_owner = create(:user)
    contributors = create_list(:user, 2)
    project = create(:project, user: project_owner)

    project.contributors << contributors

    login_as project_owner
    visit project_path(project)

    click_on 'project_contributors'

    expect(page).to have_content contributors[0].name
    expect(page).to have_content contributors[0].email

    expect(page).to have_content contributors[1].name
    expect(page).to have_content contributors[1].email
  end
end
