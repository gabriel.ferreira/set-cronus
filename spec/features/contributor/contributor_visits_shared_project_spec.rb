# frozen_string_literal: true
require 'rails_helper'

feature 'contributor visits shared project' do
  let(:project) { create(:project) }

  before do
    contributor = create(:user)
    project.contributors << contributor

    login_as contributor

    visit shared_projects_user_path(contributor)

    click_on project.name
  end

  scenario 'successfully' do
    expect(page).to have_css('h1', text: project.name)
  end

  scenario 'and do not sees project edit button' do
    expect(page).to_not have_css('a#edit_project')
  end

  scenario 'and do not sees project new link button' do
    expect(page).to_not have_css('a#link_tasks')
  end

  scenario 'and do not sees project new task button' do
    expect(page).to_not have_css('a#new_task')
  end

  scenario 'and do not sees project new contributor' do
    expect(page).to_not have_css('a#new_contributor')
  end
end
