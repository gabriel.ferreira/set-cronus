# frozen_string_literal: true
require 'rails_helper'

feature 'contributor sees assigned task' do
  scenario 'successfully' do
    assigned_task = create(:task)
    contributor = create(:user)

    assigned_task.project.contributors << contributor
    assigned_task.contributors << contributor

    login_as contributor

    visit project_path(assigned_task.project)
    click_on 'project_tasks'
    click_on assigned_task.name

    within "#task-#{assigned_task.id}" do
      expect(page).to have_content(assigned_task.name)
      expect(page).to have_content(assigned_task.description)
    end
  end
end
