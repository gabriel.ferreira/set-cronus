# frozen_string_literal: true
require 'rails_helper'

feature 'contributor sees shared projects' do
  scenario 'successfully' do
    user = create(:user)
    contributor = create(:user)
    projects = create_list(:project, 2, user: user)
    someone_else_projects = create(:project)

    projects[0].contributors << contributor

    login_as contributor

    visit user_path(contributor)

    expect(page).to_not have_content(projects[0].name)
    expect(page).to_not have_content(projects[1].name)
    expect(page).to_not have_content(someone_else_projects.name)

    click_on 'shared_projects'

    expect(page).to have_content(projects[0].name)
    expect(page).to_not have_content(projects[1].name)
    expect(page).to_not have_content(someone_else_projects.name)
  end

  scenario 'when there is no shared project' do
    user = create(:user)
    login_as user
    visit shared_projects_user_path(user)

    expect(page).to have_content('No one has shared a project with you.')
  end

  scenario 'when there is not any shared project' do
    user = create(:user)
    project = create(:project, user: user)

    contributor = create(:user)
    project.contributors << contributor

    login_as contributor
    visit shared_projects_user_path(user)

    expect(page).to_not have_content('No one has shared a project with you.')
  end
end
