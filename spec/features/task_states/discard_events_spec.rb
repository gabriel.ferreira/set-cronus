require 'rails_helper'

feature 'discard event' do
	before do
		@owner = create(:user)
		@contributor = create(:user)
		@project = create(:project, user: @owner)

		@task = create(:task, project: @project, state: 'discarded')

		login_as @owner
	end

	it 'shows current task state' do
		visit task_path(@task)	
		expect(page).to have_content('Discarded')
	end

	context 'when single user' do
		it 'can discard tasks' do
			@task.update_attributes(state: 'scheduled')

			visit task_path(@task)
			click_on 'task_discard'

			expect(page).to have_content('Discarded')
		end
	end

	context 'when multi-user' do
		before do
			@project.contributors << @contributor
		end

		it 'when has owner credentials can discard task' do
			@task.update_attributes(state: 'scheduled')

			visit task_path(@task)
			click_on 'task_discard'

			expect(page).to have_content('Discarded')
		end

		it 'when has contributor credentials cannot discard task' do
			login_as @contributor

			@task.update_attributes(state: 'scheduled')

			visit task_path(@task)

			expect(page).to_not have_link 'task_discard'
		end
	end
end