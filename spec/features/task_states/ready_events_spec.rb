require 'rails_helper'

feature 'ready event' do
	before do
		@owner = create(:user)
		@project = create(:project, user: @owner)

		@task = create(:task, project: @project, state: 'ready')

		login_as @owner
	end

	it 'shows current task state' do
		visit task_path(@task)	
		expect(page).to have_content('Ready')
	end

	context 'when task has dependencies' do
		it 'and dependencies are marked as finished it sets task as ready' do
			@task.update_attributes(state: 'started')
			@dependency = create(:task, project: @project, state: 'scheduled')
			@task.dependencies << @dependency

			visit task_path(@task)
			click_on 'task_finish'

			visit task_path(@dependency)

			expect(page).to have_content('Ready')
		end
	end

	context 'when task is created' do
		it 'creates new empty tasks as ready' do
			visit project_path(@project)
			@task = build(:task)

			click_on 'new_task'

			fill_in 'task_name', with: @task.name
			fill_in 'task_description', with: @task.description
			fill_in 'task_starting_date', with: Time.zone.now + 1.hour
			fill_in 'task_ending_date', with: @project.ending_date

			click_on 'submit'

			@task = Task.last
			visit task_path(@task)

			expect(page).to have_content('Ready')
		end
	end
end
