require 'rails_helper'

feature 'reject event' do
	before do
		@owner = create(:user)
		@project = create(:project, user: @owner)
		@contributor = create(:user)
		@project.contributors << @contributor

		@task = create(:task, project: @project, state: 'verifying')
		@task.contributors << @contributor

		login_as @owner
	end

	context 'when user is the owner' do
		it 'rejects the task' do
			visit task_path(@task)

			click_on 'task_reject'
			expect(page).to have_content('Running')
		end
	end

	context 'when user is not the owner' do
		it 'cannot reject the task' do
			login_as @contributor

			visit task_path(@task)

			expect(page).to_not have_link 'task_reject'
		end
	end
end
