require 'rails_helper'

feature 'finish event' do
	before do
		@owner = create(:user)
		@project = create(:project, user: @owner)

		@task = create(:task, project: @project, state: 'finished')

		login_as @owner
	end

	it 'shows current task state' do
		visit task_path(@task)	
		expect(page).to have_content('Finished')
	end

	context 'when task is started' do
		it 'can be set as finished' do
			@now = Time.zone.now

			allow(Time.zone)
				.to receive(:now)
				.and_return(@now)

			@task.update_attributes(state: 'started')

			visit task_path(@task)

			click_on 'task_finish'

			expect(page).to have_content 'Finished'
			expect(@task.reload.finished_at.to_i).to eq @now.to_i
		end
	end
end