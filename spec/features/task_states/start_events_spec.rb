require 'rails_helper'

feature 'start event' do
	before do
		@owner = create(:user)
		@contributor = create(:user)
		@project = create(:project, user: @owner)
		@project.contributors << @contributor

		@task = create(:task, project: @project, state: 'started')

		login_as @owner
	end

	it 'shows current task state' do

		visit task_path(@task)
		expect(page).to have_content('Running')
	end

	context 'when task is ready' do
		it 'can be updated to started' do
			@now = Time.zone.now

			allow(Time.zone)
				.to receive(:now)
				.and_return(@now)

			@task.update_attributes(state: 'ready')
			@task.contributors << @task.owner

			visit task_path(@task)

			click_on 'task_start'

			expect(page).to have_content 'Running'
			expect(@task.reload.started_at.to_i).to eq @now.to_i
		end
	end

	context 'when multi-user project' do
		context 'if task was assigned' do
			it 'can set as started ' do
				login_as @contributor

				@task.update_attributes(state: 'ready')
				@task.contributors << @contributor

				visit task_path(@task)
				expect(page).to have_button('task_start')
			end
		end

		context 'if task was not assigned' do
			it 'cannot set as started ' do
				login_as @contributor

				@task.update_attributes(state: 'ready')
				visit task_path(@task)

				expect(page).to_not have_button('task_start')
			end
		end
	end
end
