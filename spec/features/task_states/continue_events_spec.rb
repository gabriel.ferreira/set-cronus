require 'rails_helper'

feature 'continue event' do
	before do
		@owner = create(:user)
		@contributor = create(:user)
		@project = create(:project, user: @owner)

		@task = create(:task, project: @project, state: 'started')

		login_as @owner
	end

	context 'when single user' do
		it 'can continue stopped tasks' do
			@task.update_attributes(state: 'stopped')
			@task.contributors << @owner

			visit task_path(@task)
			click_on 'task_continue'

			expect(page).to have_content('Running')
		end
	end

	context 'when multi-user' do
		before do
			@project.contributors << @contributor
		end

		it 'when has owner credentials can continue task' do
			@task.update_attributes(state: 'stopped')
			@task.contributors << @owner

			visit task_path(@task)
			click_on 'task_continue'

			expect(page).to have_content('Running')
		end

		it 'when task has no contributors cannot continue task' do
			@task.update_attributes(state: 'stopped')

			visit task_path(@task)
			click_on 'task_continue'

			expect(page).to_not have_content('Running')
		end

		it 'when has contributor credentials cannot continue task' do
			login_as @contributor
			@task.update_attributes(state: 'stopped')

			visit task_path(@task)

			expect(page).to_not have_link 'task_continue'
		end
	end
end