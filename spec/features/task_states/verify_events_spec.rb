require 'rails_helper'

feature 'verify event' do
	before do
		@owner = create(:user)
		@project = create(:project, user: @owner)
		@contributor = create(:user)

		@project.contributors << @contributor

		@task = create(:task, project: @project, state: 'verifying')
		login_as @owner
	end

	it 'shows current task state' do
		visit task_path(@task)	
		expect(page).to have_content('Verifying')
	end

	context 'when task is started' do
		it 'contributor can set task as verifying' do
			login_as @contributor

			@task.update_attributes(state: 'started')
			@task.contributors = [@contributor]
			@task.save!

			visit task_path(@task)

			click_on 'task_verify'

			expect(page).to have_content 'Verifying'
		end

		it 'owner cannot set task as verifying' do
			@task.update_attributes(state: 'started')

			visit task_path(@task)

			expect(page).to_not have_link 'task_verify'
		end
	end
end