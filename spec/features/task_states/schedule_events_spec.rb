require 'rails_helper'


feature 'scheduling event' do
	before do
		@owner = create(:user)
		@project = create(:project, user: @owner)

		@task = create(:task, project: @project, state: 'scheduled')

		login_as @owner
	end

	it 'shows current task state' do
		visit task_path(@task)
		expect(page).to have_content('Scheduled')
	end

	context 'when task has dependencies' do
		context 'when task is discarded' do
			it 'when dependencies are not all finished yet sets as scheduled' do
				@task.update_attributes(state: 'discarded')
				@task.dependencies << create(:task, state: 'started', project: @project)

				visit task_path(@task)
				click_on 'task_schedule'

				expect(page).to have_content('Scheduled')
			end

			it 'when dependencies are all already finished sets as ready' do
				@task.update_attributes(state: 'discarded')
				@task.dependencies << create(:task, state: 'finished',
																						project: @project)

				visit task_path(@task)
				click_on 'task_schedule'

				expect(page).to have_content('Ready')
			end
		end

		context 'when task is ready and a dependency is added' do
			it 'schedules the task' do
				@task.update_attributes(state: 'ready')
				@dependency = create(:task, project: @project, state: 'ready')

				visit project_path(@project)

				click_on 'link_tasks'

				select @task.name, from: 'task_requisite'
				select @dependency.name, from: 'task_dependency'

				click_on 'submit'

				visit task_path(@dependency)

				expect(page).to have_content('Scheduled')
			end
		end
	end

	context 'when task has no dependencies' do
		it 'when task is discarded it is set as ready' do
			@task.update_attributes(state: 'discarded')

			visit task_path(@task)
			click_on 'task_schedule'

			expect(page).to have_content('Ready')
		end
	end
end
