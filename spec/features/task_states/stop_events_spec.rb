require 'rails_helper'

feature 'stop event' do
	before do
		@owner = create(:user)
		@contributor = create(:user)
		@project = create(:project, user: @owner)

		@task = create(:task, project: @project, state: 'stopped')

		login_as @owner
	end

	it 'shows current task state' do
		visit task_path(@task)
		expect(page).to have_content('Stopped')
	end

	context 'when single user' do
		it 'can stop tasks' do
			@task.update_attributes(state: 'started')

			visit task_path(@task)
			click_on 'task_stop'

			expect(page).to have_content('Stopped')
		end
	end

	context 'when multi-user project' do
		context 'if task was assigned' do
			it 'can set as stopped' do
				@project.contributors << @contributor
				login_as @contributor

				@task.update_attributes(state: 'started')
				@task.contributors << @contributor

				visit task_path(@task)
				expect(page).to have_button('task_stop')
			end
		end

		context 'if task was not assigned' do
			it 'cannot set as stopped' do
				@project.contributors << @contributor
				login_as @contributor

				@task.update_attributes(state: 'started')
				visit task_path(@task)

				expect(page).to_not have_button('task_stop')
			end
		end

		context 'if assigned contributor is removed' do
			it 'is set as stopped' do
				@project.contributors << @contributor
				@task.contributors = [@contributor]
				@task.save!

				visit project_contributors_path(@project)

				within "#contributor-#{@contributor.id}" do
					click_on 'delete_contributor'
				end

				visit task_path(@task)
				expect(page).to have_content('Stopped')
			end
		end
	end
end
