require 'rails_helper'

describe TaskButtonsGenerator do
	describe 'event buttons' do
		it 'calls single user panel when project is single user' do
			expect_any_instance_of(TaskButtons::SingleUserPanel)
			  .to receive(:render)

			task = build(:task)
			described_class.new(task: task, user: task.owner).event_buttons
		end

		context 'when project is multi-user' do
			before do
				@task = build(:task)
				@contributor = build(:user)

				@task.project.contributors << @contributor
			end

			it 'calls contributors panel when user is a contributor' do
				expect_any_instance_of(TaskButtons::ContributorPanel)
					.to receive(:render)

				@task.contributors << @contributor

				described_class.new(task: @task, user: @contributor).event_buttons
			end

			it 'calls project manager panel when user is project owner' do
				expect_any_instance_of(TaskButtons::ProjectManagerPanel)
					.to receive(:render)

				described_class.new(task: @task, user: @task.owner).event_buttons
			end

			it 'do not calls renderer if contributor is not assigned' do
				expect_any_instance_of(TaskButtons::ContributorPanel)
					.to_not receive(:render)

				expect_any_instance_of(TaskButtons::ProjectManagerPanel)
					.to_not receive(:render)

				return_value = described_class.new(task: @task, user: @contributor).event_buttons
				expect(return_value).to be_a(Array)
			end
		end

		it 'do not calls any renderer otherwise' do
			expect_any_instance_of(TaskButtons::SingleUserPanel)
			  .to_not receive(:render)

			expect_any_instance_of(TaskButtons::ContributorPanel)
				.to_not receive(:render)

			expect_any_instance_of(TaskButtons::ProjectManagerPanel)
				.to_not receive(:render)

			return_value = described_class.new(task: build(:task), user: build(:user)).event_buttons

			expect(return_value).to be_a(Array)
		end
	end
end
