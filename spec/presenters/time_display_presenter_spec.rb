# frozen_string_literal: true
require 'rails_helper'

describe TimeDisplayPresenter do
  subject { described_class.new project }

  let(:project) { double }
  let(:starting_time) { Time.zone.now }

  describe '#time_remaining' do
    it 'calculates the remaining days time project deadline' do
      allow(project)
        .to receive(:starting_date)
        .and_return starting_time

      allow(project)
        .to receive(:ending_date)
        .and_return starting_time + 15.days

      expect(subject.time_remaining).to eq '15 days remaining'
    end
  end
end
