require 'rails_helper'

describe TaskButtons::SingleUserPanel do
	before do
		@task = create(:task)
		@user = create(:user)
	end

	describe '#render' do
		context 'when task is scheduled' do
			it 'renders corresponding buttons' do
				@task.update_attributes(state: 'scheduled')
				renderer = described_class.new(@task)

        expect(renderer.render.first[:route])
				  .to match /discard/
			end
		end

		context 'when task is ready' do
			it 'renders corresponding buttons' do
				@task.update_attributes(state: 'ready')
				renderer = described_class.new(@task)

        buttons = renderer.render

				expect(buttons[0][:route]).to match /start/
        expect(buttons[1][:route]).to match /schedule/
        expect(buttons[2][:route]).to match /discard/
			end
		end

		context 'when task is started' do
			it 'renders corresponding buttons' do
				@task.update_attributes(state: 'started')
				renderer = described_class.new(@task)

        buttons = renderer.render

        expect(buttons[0][:route]).to match /finish/
        expect(buttons[1][:route]).to match /schedule/
        expect(buttons[2][:route]).to match /stop/
			end
		end

		context 'when task is stopped' do
			it 'renders corresponding buttons' do
				@task.update_attributes(state: 'stopped')
				renderer = described_class.new(@task)

        buttons = renderer.render

        expect(buttons[0][:route]).to match /continue/
        expect(buttons[1][:route]).to match /discard/
			end
		end

		context 'when task is finished' do
			it 'renders corresponding buttons' do
				@task.update_attributes(state: 'finished')
				renderer = described_class.new(@task)

        buttons = renderer.render

				expect(renderer.render).to be_empty
			end
		end

		context 'when state is discarded' do
			it 'renders nothing' do
				@task.update_attributes(state: 'discarded')
				renderer = described_class.new(@task)

        buttons = renderer.render

        expect(buttons[0][:route]).to match /schedule/
      end
		end
	end
end
