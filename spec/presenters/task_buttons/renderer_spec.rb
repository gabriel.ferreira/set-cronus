require 'rails_helper'

describe TaskButtons::Renderer do
  describe '#render' do
    it 'renders a single task event button' do
      rendered = described_class.new(events: ['start'],
                                     id: 1).render

      expect(rendered).to eq [{
        route: '/tasks/1/start',
        method: :post,
        id: 'task_start',
        name: 'Start'
      }]
    end

    it 'renders several event buttons' do
      events_array = %w(start finish ready)
      rendered = described_class.new(events: events_array,
                                     id: 1).render

      expected_output = [
        { route: '/tasks/1/start',
          method: :post,
          id: 'task_start',
          name: 'Start' },
        { route: '/tasks/1/finish',
          method: :post,
          id: 'task_finish',
          name: 'Finish' },
        { route: '/tasks/1/ready',
          method: :post,
          id: 'task_ready',
          name: 'Ready' }
      ]

      expect(rendered).to eq expected_output
    end

    it 'returns empty array when no events' do
      rendered = described_class.new(events: [], id: 1).render
      expect(rendered).to eq []
    end
  end
end
