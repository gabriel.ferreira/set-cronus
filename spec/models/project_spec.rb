# frozen_string_literal: true
require 'rails_helper'

describe Project do
  subject { build(:project) }

  it { should validate_presence_of :name }
  it { should validate_presence_of :starting_date }
  it { should validate_presence_of :ending_date }
  it { should belong_to :user }
  it { should have_many :task_reports }

  it do
    should have_many(:contributors)
      .through(:project_contributors)
      .source(:user)
  end

  it 'aliases user to owner' do
    expect(subject.owner).to eq subject.user
  end

  describe '#in_trash?' do
    it 'returns true when in trash' do
      project = described_class.new(in_trash: true)
      expect(project.in_trash?).to be true
    end

    it 'returns false when is not in trash' do
      project = described_class.new(in_trash: false)
      expect(project.in_trash?).to be false
    end

    it 'returns false by default for new projects' do
      expect(described_class.new.in_trash?).to be false
    end
  end
end
