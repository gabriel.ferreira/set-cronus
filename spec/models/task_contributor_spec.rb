# frozen_string_literal: true
require 'rails_helper'

describe TaskContributor do
  it { should belong_to :user }
  it { should belong_to :task }
end
