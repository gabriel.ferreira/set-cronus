# frozen_string_literal: true
require 'rails_helper'

describe Task do
  subject { build(:task) }

  it { should validate_presence_of :name }
  it { should validate_presence_of :starting_date }
  it { should validate_presence_of :ending_date }
  it { should validate_presence_of :project_id }
  it { should have_many(:task_reports) }
  it { should belong_to :project }

  it do
    should have_many(:dependencies)
      .through(:task_dependencies)
      .source(:dependency)
  end


  it do
    should have_many(:contributors)
      .through(:task_contributors)
      .source(:user)
  end

  it 'belongs to a user' do
    expect(subject.owner).to eq subject.project.owner
  end

  it 'has dependencies' do
    should have_many :dependencies
  end

  describe '#no_dependencies?' do
    it 'returns true when no dependencies' do
      expect(subject.no_dependencies?).to be true
    end

    it 'returns false when has dependencies' do
      subject.dependencies << build(:task)
      expect(subject.no_dependencies?).to be false
    end
  end

  context 'when normal association' do
    let(:task_a) { create(:task, project: project) }
    let(:task_b) { create(:task, project: project) }
    let(:project) { create(:project) }

    it 'relates dependencies and requisites' do
      task_a.dependencies << task_b
      task_a.save

      expect(task_a.dependencies).to include(task_b)
      expect(task_b.reload.requisites).to include(task_a)
    end
  end

  it 'has states' do
    task = build(:task)
    expect(task.state).to eq 'ready'
  end

  describe '#assigned_to?' do
    it 'returns true when task was assigned to user' do
      user = create(:user)
      subject.contributors << user

      expect(subject.assigned_to?(user)).to be true
    end

    it 'returns false otherwise' do
      user = create(:user)

      expect(subject.assigned_to?(user)).to be false
    end
  end

  # Adopted the metaprogramming approach, instead of the
  # more verbose and clear way, in order to keep the tests
  # of task state related methods up-to-date with WorkStates
  # whenever a new task is added.
  StateMachines::WorkStates::STATES.each do |state|
    describe "##{state}?" do
      it "returns true when status is #{state}" do
        subject.state = state
        expect(subject.send("#{state}?")).to eq true
      end

      it 'returns false for anything else' do
        subject.state = 'anything else'
        expect(subject.send("#{state}?")).to eq false
      end
    end
  end
end
