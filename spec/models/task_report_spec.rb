require 'rails_helper'

describe TaskReport do
  it { should validate_presence_of :original_state }
  it { should validate_presence_of :new_state }
  it { should belong_to :project }
  it { should belong_to :user }
  it { should belong_to :task }
end
