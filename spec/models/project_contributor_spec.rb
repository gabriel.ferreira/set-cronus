# frozen_string_literal: true
require 'rails_helper'

describe ProjectContributor do
  it { should belong_to :user }
  it { should belong_to :project }
end
