# frozen_string_literal: true
require 'rails_helper'

describe User do
  it { should validate_uniqueness_of :name }
  it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }
  it { should have_many :task_reports }

  it do
    should have_many(:third_person_projects)
      .through(:project_contributors)
      .source(:project)
  end

  it do
    should have_many(:working_tasks)
      .through(:task_contributors)
      .source(:task)
  end
end
