require 'rails_helper'

describe Notification do
  it { should validate_presence_of :description }
  it { should validate_presence_of :notification_type }
  it { should belong_to :project }
  it { should belong_to :user }
  it { should belong_to :task }

  describe '#seen?' do
    it 'is false by default' do
      expect(subject.seen?).to eq false
    end

    it 'is true when set as true' do
      subject.seen = true
      expect(subject.seen?).to eq true
    end

    it 'is false when set as false' do
      subject.seen = false
      expect(subject.seen?).to eq false
    end
  end
end
