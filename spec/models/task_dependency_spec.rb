# frozen_string_literal: true
require 'rails_helper'

describe TaskDependency do
  it do
    should belong_to(:requisite)
      .class_name('Task')
      .with_foreign_key(:task_id)
  end

  it do
    should belong_to(:dependency)
      .class_name('Task')
      .with_foreign_key(:dependency_id)
  end
end
