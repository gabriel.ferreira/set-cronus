SimpleCov.start 'rails' do
	add_filter '/test/' 
	add_filter '/vendor/'
	add_filter '/db/'
	add_filter '/public/'
	add_filter '/config/'
	add_filter '/log/'

	add_group 'Controllers', 'app/controllers'
	add_group 'Views', 'app/views'
	add_group 'Services', 'app/services'
	add_group 'Validators', 'app/validators'
	add_group 'Models', 'app/models'
	add_group 'State Machines', 'app/services/state_machines'
	add_group 'Presenters', 'app/presenters'
end
