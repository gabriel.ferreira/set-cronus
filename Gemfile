# frozen_string_literal: true
source 'https://rubygems.org'

gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'therubyracer'
gem 'chartkick'
gem 'rufus-scheduler'

gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'listen', '~> 3.0.5'
gem 'devise'

gem 'rails-assets-bootstrap', source: 'https://rails-assets.org'

group :development, :test do
  gem 'byebug', platform: :mri
end

group :development do
  gem 'web-console'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'capistrano'
  gem 'capistrano-rails'
end

group :test do
  gem 'rubycritic', require: false
  gem 'rubocop', require: false
  gem 'simplecov', require: false
  gem 'rspec-rails', '~> 3.5'
  gem 'rails-controller-testing'
  gem 'capybara'
  gem 'shoulda-matchers'
  gem 'factory_girl_rails'
end
