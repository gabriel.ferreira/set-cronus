require 'rufus-scheduler'

s = Rufus::Scheduler.singleton

s.every '12h' do
  print 'Running notifications generator for late tasks...'
  tasks = LateTasksFinder.new.find
  LateTaskNotificationCreator.new.run(tasks)
  puts 'Done'
end
