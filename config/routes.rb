Rails.application.routes.draw do
  devise_for :users
  root 'home#index'

  resources :users, only: [:show] do
  	get 'shared_projects', on: :member
	  resources :projects, only: [:index, :new, :create]
    resources :trashes, only: [:index]
    resources :notifications, only: [:index, :destroy]
  end

  resources :projects, only: [:show, :edit, :update, :destroy] do
  	resources :tasks, only: [:new, :create, :index]
  	resources :contributors, only: [:new, :create, :index, :destroy]

    resources :links, only: [:new, :create] do
      get 'unlink', on: :member
    end

    resources :trashes, only: [:create] do
      collection { post 'restore', to: 'trashes#restore' }
    end
  end

  resources :tasks, only: [:edit, :update, :show] do
    resources :assignees, only: [:new, :create]
    post 'start', to: 'task_states#start'
    post 'ready', to: 'task_states#ready'
    post 'finish', to: 'task_states#finish'
    post 'accept', to: 'task_states#accept'
    post 'schedule', to: 'task_states#schedule'
    post 'stop', to: 'task_states#stop'
    post 'continue', to: 'task_states#continue'
    post 'verify', to: 'task_states#verify'
    post 'reject', to: 'task_states#reject'
    post 'discard', to: 'task_states#discard'
  end

  resources :links, only: [:destroy]
end
