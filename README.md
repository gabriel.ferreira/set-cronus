# Projeto Set Cronus

Gerenciador de cronograma de atividades.

## Colaboradores
* Gabriel Ferreira - sp1462687
* Celso Hashisaka Junior - sp127077X
* Daniel Pavan - sp1465988

## Colaborando com o projeto:
* Instale o git em sua máquina
* Execute git clone https://gitlab.com/setcronus/set-cronus.git
* Configure o git com seu nome de usuário git config --global user.name
* Configure o git com seu prontuário ou email git config --global user.email

## Sobre o projeto

### Versão do Ruby
A versão utilizada para este projeto é a 2.3.1

### Dependências do sistema
* Postgres 9.5 ou mais recente

### Configuração
Executar `bundle install` na raiz do projeto para fazer download das gems do projeto.

### Setup do banco
Executar `bundle exec rails db:create db:migrate`

### Executando o projeto
Executar `bundle exec rails s`
Caso esteja rodando no vagrant, use `bundle exec rails s -b 0.0.0.0`

### Executando os testes da aplicação
Executar `bundle exec rspec`

## Colaborando com o projeto

### Atualizar projeto local com o repositório
`git pull origin master`
Se você tiver alterações locais ainda não comitadas, use `git stash` 
antes de fazer o pull, e depois do pull, rode `git stash pop`

### Subir alterações
´git push origin master`
É importante lembrar que se a Versão local estiver desatualizada em relação
ao repo, é preciso fazer um pull, resolver os conflitos locais, e depois fazer o
push.

### Adicionar suas alterações no histórico do git
`git add <arquivos alterados>`
`git commit -m <mensagem de commit>`
