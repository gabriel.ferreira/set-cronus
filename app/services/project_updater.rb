# frozen_string_literal: true
class ProjectUpdater
  def initialize(project)
    @project = project
    @validations = [
      MissingDeadlines,
      EndingDateBeforeToday,
      EndingBeforeStarting
    ]
  end

  def update(params)
    @date = params[:starting_date]

    validating_params = params.merge(errors: @project.errors, name: 'project')
    @validators = ValidatorRunner.new(validating_params, @validations)

    return unless @validators.valid?

    update_older_tasks! if starting_date_changed?
    @project.update(params)
  end

  private

  def starting_date_changed?
    @project.starting_date != @date
  end

  def update_older_tasks!
    @project.tasks.where('starting_date < ?', @date).each do |task|
      starting_date = @date
      ending_date = starting_date + (task.ending_date - task.starting_date)
      task.update_attributes(starting_date: starting_date, ending_date: ending_date)
    end
  end
end
