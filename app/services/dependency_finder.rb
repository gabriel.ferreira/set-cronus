# frozen_string_literal: true
class DependencyFinder
  def initialize(task)
    @task = task
    @dependencies = []
  end

  def find
    gather_dependencies(@task)
    @dependencies -= [@task]
    @dependencies.to_set
  end

  private

  def gather_dependencies(task)
    @dependencies += [task]

    return if task.no_dependencies?

    task.dependencies.each do |dependency|
      gather_dependencies(dependency)
    end
  end
end
