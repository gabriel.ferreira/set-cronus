class LateTasksFinder
  def find
    Task.where(
      'ending_date < ? AND (state != ? AND state != ?)',
      Time.zone.now.beginning_of_day,
      'finished',
      'discarded')

  end
end
