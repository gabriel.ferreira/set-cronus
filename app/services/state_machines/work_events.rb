require "#{Rails.root}/lib/config_loading"

module StateMachines
  module WorkEvents
    EVENTS = load_config('work_state_machines.yml')['events']
    EVENTS.each { |event| const_set(event.upcase, event)}
  end
end
