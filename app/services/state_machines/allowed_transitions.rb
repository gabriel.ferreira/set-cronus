module StateMachines
	class AllowedTransitions
		include WorkEvents

		def initialize(args)
      @task = args.fetch(:record)
			@state = @task.state
    end

		def allowed_transitions
			EVENTS.find_all do |event|
				klass = eval("Events::#{event.capitalize}")
				klass.new(record: @task, user: @task.owner).allowed_transition?
			end
		end
	end
end
