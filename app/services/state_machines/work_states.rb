# frozen_string_literal: true
require "#{Rails.root}/lib/config_loading"

module StateMachines
  module WorkStates
    STATES = load_config('work_state_machines.yml')['states']
    STATES.each { |state| const_set(state.upcase, state)}
  end
end
