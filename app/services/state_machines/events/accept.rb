module StateMachines
	module Events
		class Accept < Base
			def new_state
				FINISHED
			end

			def allowed_original_states
				[VERIFYING]
			end

			def custom_validations
				@task.project.contributors.any?
			end
		end
	end
end