# frozen_string_literal: true
module StateMachines
  module Events
    class Schedule < Base
      def new_state
        SCHEDULED
      end

      def allowed_original_states
        [ READY, STARTED, DISCARDED ]
      end
    end
  end
end
