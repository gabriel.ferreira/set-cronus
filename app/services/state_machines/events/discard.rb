module StateMachines
	module Events
		class Discard < Base
			def new_state
				DISCARDED
			end

			def allowed_original_states
				[SCHEDULED, VERIFYING, READY, STOPPED]
			end
		end
	end
end