# frozen_string_literal: true
module StateMachines
  module Events
    class Stop < Base
      def new_state
        STOPPED
      end

      def allowed_original_states
         [STARTED]
      end
    end
  end
end
