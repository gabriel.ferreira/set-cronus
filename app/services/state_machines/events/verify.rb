# frozen_string_literal: true
module StateMachines
  module Events
    class Verify < Base
      def new_state
        VERIFYING
      end

      def allowed_original_states
        [STARTED]
      end

      def custom_validations
        @task.project.contributors.any?
      end
    end
  end
end
