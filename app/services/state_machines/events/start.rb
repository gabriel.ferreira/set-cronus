# frozen_string_literal: true
module StateMachines
  module Events
    class Start < Base
      def new_state
        STARTED
      end

      def allowed_original_states
        [READY]
      end
    end
  end
end
