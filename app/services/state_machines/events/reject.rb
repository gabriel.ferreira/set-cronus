# frozen_string_literal: true
module StateMachines
  module Events
    class Reject < Base
      def new_state
        STARTED
      end

      def allowed_original_states
        [VERIFYING]
      end

      def custom_validations
        @task.project.contributors.any?
      end
    end
  end
end
