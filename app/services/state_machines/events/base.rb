# frozen_string_literal: true
module StateMachines
  module Events
    class Base
      include WorkStates

      def initialize(args)
        @task = args.fetch(:record)
        @user = args.fetch(:user)
        @original_state = @task.state
        @callbacks = args.fetch(:callbacks, [])

        @callbacks << Callbacks::TaskStateReports
        @callbacks << Callbacks::StatusChangedNotification
      end

      def change_state
        if allowed_transition?
          @task.update_attributes(state: new_state)
          run_callbacks
        end
      end

      def allowed_original_states
        []
      end

      def custom_validations
        true
      end

      def new_state
        raise NotImplementedError
      end

      def allowed_transition?
        allowed_state? && custom_validations
      end

      private

      def run_callbacks
        transition_args = {record: @task,
                           user: @user,
                           original_state: @original_state,
                           new_state: new_state}

        @callbacks.each do |callback|
          callback.new(transition_args).run
        end
      end

      def allowed_state?
        allowed_original_states.include?(@original_state)
      end
    end
  end
end
