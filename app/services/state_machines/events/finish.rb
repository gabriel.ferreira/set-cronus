# frozen_string_literal: true
module StateMachines
  module Events
    class Finish < Base
      def new_state
        FINISHED
      end

      def allowed_original_states
        [STARTED]
      end

      def custom_validations
        @task.project.contributors.empty?
      end
    end
  end
end
