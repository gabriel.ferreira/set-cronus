# frozen_string_literal: true
module StateMachines
  module Events
    class Ready < Base
      def new_state
        READY
      end

      def allowed_original_states
        [ SCHEDULED, 
          STARTED,
          DISCARDED ]
      end

      def custom_validations
        can_be_ready?
      end

      private

      def can_be_ready?
        DependencyFinder.new(@task).find.all? do |dependency|
          dependency.state == 'finished'
        end
      end
    end
  end
end
