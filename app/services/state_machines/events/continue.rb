# frozen_string_literal: true
module StateMachines
  module Events
    class Continue < Base
      def new_state
        STARTED
      end

      def allowed_original_states
         [STOPPED]
      end
    end
  end
end
