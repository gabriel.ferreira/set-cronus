# frozen_string_literal: true
module StateMachines
  class Work
    include WorkEvents

    def initialize(args)
      @args = args
    end

    EVENTS.each do |state|
      define_method(state) do
        call_change_state(state)
      end
    end

    private

    def call_change_state(state)
      klass = eval("Events::#{state.capitalize}")
      klass.new(@args).change_state
    end
  end
end
