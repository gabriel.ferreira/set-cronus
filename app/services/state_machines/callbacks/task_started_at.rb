module StateMachines
	module Callbacks
		class TaskStartedAt
			def initialize(args)
        @task = args.fetch(:record)
			end

			def run
				@task.update_attributes(started_at: Time.zone.now) if never_started?
			end

			private

			def never_started?
				@task.started_at.nil?
			end
		end
	end
end
