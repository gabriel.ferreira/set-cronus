module StateMachines
  module Callbacks
    class TaskStateReports
      def initialize(args)
        @task = args.fetch(:record)
        @original_state = args.fetch(:original_state)
        @new_state = args.fetch(:new_state)
        @user = args.fetch(:user)
      end

      def run
        TaskReport.create(
          task: @task,
          project: @task.project,
          user: @user,
          new_state: @new_state,
          original_state: @original_state)
      end
    end
  end
end
