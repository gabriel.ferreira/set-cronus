module StateMachines
	module Callbacks
		class TaskFinishedAt
			def initialize(args)
        @task = args.fetch(:record)
			end

			def run
				@task.update_attributes(finished_at: Time.zone.now)
			end
		end
	end
end
