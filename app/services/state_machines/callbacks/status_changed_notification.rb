module StateMachines
  module Callbacks
    class StatusChangedNotification
      def initialize(args)
        @task = args.fetch(:record)
        @user = args.fetch(:user)
        @original_state = args.fetch(:original_state)
        @new_state = args.fetch(:new_state)
      end

      def run
        Notification.create(
          task: @task,
          project: @task.project,
          user: @user,
          notification_type: 'status_changed',
          description: description)
      end

      private

      def description
        I18n.t('notification.status_changed.description',
               user: @user.name,
               task: @task.name,
               original_state: @original_state,
               new_state: @new_state,
               date: time_log
              )
      end

      def time_log
        I18n.l(Time.zone.now, format: :long)
      end
    end
  end
end
