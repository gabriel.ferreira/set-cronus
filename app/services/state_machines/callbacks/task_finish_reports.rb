module StateMachines
  module Callbacks
    class TaskFinishReports
      SECONDS_BY_DAY = 60 * 60 * 24

      def initialize(args)
        @task = args.fetch(:record)
        @user = args.fetch(:user)
      end

      def run
        TaskFinishReport.create!(
          task: @task,
          project: @task.project,
          delayed_time: delayed_time,
          user: @user)
      end

      private

      def delayed_time
        convert_to_days(ending_date - today)
      end

      def convert_to_days(date_in_seconds)
        (date_in_seconds.to_f / SECONDS_BY_DAY ).round
      end

      def ending_date
        @task.ending_date
      end

      def today
        Time.zone.now
      end
    end
  end
end
