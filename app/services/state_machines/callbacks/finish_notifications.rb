module StateMachines
  module Callbacks
    class FinishNotifications
      def initialize(args)
        @task = args.fetch(:record)
        @user = args.fetch(:user)
      end

      def run
        Notification.create(
          task: @task,
          project: @task.project,
          user: @task.owner,
          notification_type: 'task_finished',
          description: description
        )
      end

      private

      def description
        I18n.t(
          'notification.task_finished.description',
          user: @user.name,
          task: @task.name,
          date: time_log
        )
      end

      def time_log
        I18n.l(Time.zone.now, format: :long)
      end
    end
  end
end
