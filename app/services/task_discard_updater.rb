class TaskDiscardUpdater
  def initialize(task, user)
    @task = task
    @user = user
    @state_machine = StateMachines::Work.new(record: task, user: user)
    @dependencies = DependencyFinder.new(task).find
  end

  def update
    @state_machine.discard
    @task.contributors.destroy_all

    remove_task_from_requisites_dependencies
    release_task_dependencies
  end

  private

  def remove_task_from_requisites_dependencies
    @task.requisites.each do |task_dependency|
      task_dependency.dependencies.delete(@task)
    end
  end

  def release_task_dependencies
    @task.dependencies.each do |dependency|
      state_machine = StateMachines::Work.new(record: dependency,
                                              user: @user)

      state_machine.ready if dependency.scheduled?
      state_machine.continue if dependency.stopped?
    end

    @task.dependencies.destroy_all
  end
end
