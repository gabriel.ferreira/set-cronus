class LinkCreator
  def initialize(args)
  	@dependency = args.fetch(:dependency)
  	@requisite = args.fetch(:requisite)
    @validator = args.fetch(:validator)
    @user = args.fetch(:user)
    @state_machine = StateMachines::Work.new(record: @dependency, user: @user)
  end

  def create
    return if linking_not_allowed?

    @requisite.dependencies << @dependency
    update_dependency_status
  end

  private

  def linking_not_allowed?
    @dependency.finished? || !@validator.valid?
  end

  def update_dependency_status
    if @dependency.ready?
      @state_machine.schedule
    elsif @dependency.started?
      @state_machine.stop
    end
  end
end
