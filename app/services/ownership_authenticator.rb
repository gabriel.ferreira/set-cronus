# frozen_string_literal: true
class OwnershipAuthenticator
  def initialize(args = {})
    @resource = args.fetch(:resource)
    @user = args.fetch(:user)
  end

  def owner?
    return false if user_not_logged_in?
    @resource.owner.id == @user.id
  end

  def allowed_in?
    return false if user_not_logged_in?
    owner? || contributor?
  end

  private

  def user_not_logged_in?
    @user.nil?
  end

  def contributor?
    @resource.contributors.include? @user
  end
end
