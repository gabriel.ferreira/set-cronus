# frozen_string_literal: true
class LinksValidator
  VALIDATORS = [
    Links::DependenciesValidator,
    Links::EqualityValidator,
    Links::ModelValidator,
    Links::SameProjectValidator
  ].freeze

  def initialize(task, dependency)
    @task = task
    @dependency = dependency
    @errors = {}
   end

  def errors
    check_linking_errors
    @errors
   end

  def valid?
    @errors.empty?
   end

  private

  def check_linking_errors
    VALIDATORS.each do |validator|
      validator = validator.new(task: @task, dependency: @dependency)

      @errors.merge!(validator.errors)
    end
   end
end
