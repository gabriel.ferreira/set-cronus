class TaskFinishUpdater
  def initialize(task, user)
    @dependencies = task.dependencies
    @user = user

    @state_machine = StateMachines::Work.new(record: task,
                                             user: user,
                                             callbacks: callbacks)
  end

  def update
    @state_machine.finish
    DependencyReadyUpdater.new(@dependencies, @user).update
  end

  def callbacks
    [ StateMachines::Callbacks::TaskFinishedAt,
      StateMachines::Callbacks::FinishNotifications,
      StateMachines::Callbacks::TaskFinishReports ]
  end
end
