class DependencyReadyUpdater
	def initialize(dependents, user)
		@dependents = dependents
    @user = user
	end

	def update
		@dependents.each do |dependent|
			requisites = dependent.requisites

			if requisites.empty? || requisites.all?(&:finished?)
				StateMachines::Work.new(record: dependent, user: @user).ready
			end
		end
	end
end
