class TaskScheduleUpdater
	def initialize(task, user)
		@state_machine = StateMachines::Work.new(record: task, user: user)
		@dependencies = DependencyFinder.new(task).find
	end

	def update
		if no_dependencies? || dependencies_concluded?
			@state_machine.ready
		else
			@state_machine.schedule
		end
	end

	private

	def no_dependencies?
		@dependencies.empty?
	end

	def dependencies_concluded?
		@dependencies.all? { |dependency| dependency.finished? }
	end
end
