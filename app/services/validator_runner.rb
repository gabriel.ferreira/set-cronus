# frozen_string_literal: true
class ValidatorRunner
  attr_reader :errors

  def initialize(params, validations)
    @params = params
    @validations = validations
  end

  def valid?
    @validations.each do |validation|
      validation.new(@params).validate
      return if errors?
    end

    true
  end

  private

  def errors?
    @params[:errors].full_messages.any?
  end
end
