class Notification < ActiveRecord::Base
  validates :description, presence: true
  validates :notification_type, presence: true

  belongs_to :project
  belongs_to :user
  belongs_to :task
end
