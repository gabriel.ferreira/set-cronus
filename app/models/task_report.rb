class TaskReport < ActiveRecord::Base
  validates :original_state, presence: true
  validates :new_state, presence: true

  belongs_to :project
  belongs_to :user
  belongs_to :task
end
