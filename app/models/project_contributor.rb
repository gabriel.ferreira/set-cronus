# frozen_string_literal: true
class ProjectContributor < ActiveRecord::Base
  belongs_to :user
  belongs_to :project
end
