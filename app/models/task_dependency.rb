# frozen_string_literal: true
class TaskDependency < ActiveRecord::Base
  belongs_to :requisite, class_name: 'Task', foreign_key: :task_id
  belongs_to :dependency, class_name: 'Task', foreign_key: :dependency_id
end
