# frozen_string_literal: true
class Project < ApplicationRecord
  include ActiveModel::Validations

  validates :name, presence: true
  validates :starting_date, presence: true
  validates :ending_date, presence: true

  belongs_to :user
  has_many :tasks, dependent: :destroy
  has_many :task_reports, dependent: :destroy

  has_many :project_contributors, dependent: :destroy
  has_many :contributors, through: :project_contributors,
                          source: :user,
                          dependent: :destroy

  alias_attribute :owner, :user
end
