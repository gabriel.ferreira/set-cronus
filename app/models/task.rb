# frozen_string_literal: true
class Task < ApplicationRecord
  validates :name, presence: true
  validates :starting_date, presence: true
  validates :ending_date, presence: true
  validates :project_id, presence: true

  belongs_to :project
  has_many :task_reports
  has_many :task_dependencies
  has_many :dependencies, through: :task_dependencies, source: :dependency

  has_many :task_contributors
  has_many :contributors, through: :task_contributors, source: :user

  alias_attribute :owner, :user

  def owner
    project.user
  end

  def requisites
    TaskDependency.where(dependency_id: id).map(&:requisite)
  end

  def no_dependencies?
    dependencies.empty?
  end

  def assigned_to?(user)
    contributors.include? user
  end

  StateMachines::WorkStates::STATES.each do |work_state|
    define_method("#{work_state}?") do
      state == work_state
    end
  end
end
