# frozen_string_literal: true
class TaskContributor < ActiveRecord::Base
  belongs_to :user
  belongs_to :task
end
