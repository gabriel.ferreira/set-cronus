# frozen_string_literal: true
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :projects
  has_many :task_reports

  has_many :project_contributors
  has_many :third_person_projects, through: :project_contributors,
                                   source: :project

  has_many :task_contributors
  has_many :working_tasks, through: :task_contributors,
                           source: :task

  validates :name, uniqueness: true
end
