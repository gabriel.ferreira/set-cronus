# frozen_string_literal: true
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def find_notifications
    @notifications = Notification
      .where(user: current_user)
      .where(seen: false)
      .order(created_at: :desc)

    @notifications_count = @notifications.count
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  private

  def after_sign_in_path_for(_resource)
    user_url(current_user)
  end

  def after_sign_out_path_for(_resource)
    root_url
  end

  def validate_ownership!(resource)
    authenticator = OwnershipAuthenticator.new(resource: resource,
                                               user: current_user)
    return if authenticator.owner?

    render file: 'public/404.html'
  end

  def validate_entry!(resource)
    auth = OwnershipAuthenticator.new(resource: resource, user: current_user)
    return if auth.allowed_in?

    render status: 403, file: 'public/403.html'
  end
end
