class TaskStatesController < ApplicationController
	before_action :authenticate_user!
	before_action :find_task

	def start
		if @task.assigned_to?(current_user)
			callback = StateMachines::Callbacks::TaskStartedAt
			StateMachines::Work.new(record: @task, user: current_user,
                              callbacks: [callback]).start
		end

		redirect_to task_path(@task)
	end

	def schedule
		TaskScheduleUpdater.new(@task, current_user).update
		redirect_to task_path(@task)
	end

	def finish
		TaskFinishUpdater.new(@task, current_user).update
		redirect_to task_path(@task)
	end

	def accept
		TaskAcceptUpdater.new(@task, current_user).update
		redirect_to task_path(@task)
	end

	def reject
		StateMachines::Work.new(record: @task, user: current_user).reject
		redirect_to task_path(@task)
	end

	def stop
		StateMachines::Work.new(record: @task, user: current_user).stop
		redirect_to task_path(@task)
	end

	def discard
    TaskDiscardUpdater.new(@task, current_user).update
		redirect_to task_path(@task)
	end

	def continue
		if @task.contributors.any?
			StateMachines::Work.new(record: @task, user: current_user).continue
		end

		redirect_to task_path(@task)
	end

	def verify
		StateMachines::Work.new(record: @task, user: current_user).verify
		redirect_to task_path(@task)
	end

	private

	def find_task
		@task = Task.find(params[:task_id])
	end
end
