# frozen_string_literal: true
class AssigneesController < ApplicationController
  layout 'task'

  before_action :authenticate_user!
  before_action :find_task
  before_action :find_notifications

  def new
    contributors_available = @task.project.contributors
    @contributors = contributors_available.pluck(:name, :id)
  end

  def create
    contributor = @task.project.contributors.find(params[:assignee])

    remove_current_user
    @task.contributors << contributor

    redirect_to project_tasks_path(@task.project, @task)
  end

  private

  def find_task
    @task = Task.find params[:task_id]
    validate_ownership!(@task)
  end

  def remove_current_user
    return unless @task.assigned_to?(current_user)

    @task.contributors.delete(current_user)
  end
end
