# frozen_string_literal: true
class ContributorsController < ApplicationController
  layout 'project'

  before_action :authenticate_user!
  before_action :find_project_and_validate_access
  before_action :find_notifications

  def index
    @contributors = @project.contributors
  end

  def new; end

  def create
    @invited = User.find_by_name!(params[:contributor_name])

    @project.contributors << @invited
    redirect_to action: :index
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = I18n.t('contributor.errors.not_found')
    render 'new'
  end

  def destroy
    @contributor = User.find(params[:id])

    @contributor.working_tasks.each do |task|
      StateMachines::Work.new(record: task, user: current_user).stop
    end

    @project.contributors.destroy(@contributor)

    flash[:notice] = I18n.t('contriutor.deleted.success')

    redirect_to action: :index
  rescue ActiveRecord::RecordNotFound
    render status: 404, file: 'public/404.html'
  end

  private

  def find_project_and_validate_access
    @project = Project.find params[:project_id]
    validate_ownership!(@project)
  end
end
