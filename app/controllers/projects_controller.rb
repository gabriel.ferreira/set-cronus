# frozen_string_literal: true
class ProjectsController < ApplicationController
  layout :default_layout

  before_action :authenticate_user!, except: [:show, :index]
  before_action :find_project, only: [:edit, :update, :show, :destroy]
  before_action :find_notifications

  def index
    @projects = current_user
      .projects
      .where(in_trash: false)
      .map { |project| TimeDisplayPresenter.new(project) }
  end

  def new
    @project = Project.new
  end

  def create
    @project = current_user.projects.build(project_params)

    if valid_deadline? && @project.save
      flash[:notice] = I18n.t('project.created.success')

      redirect_to project_path(@project)
    else
      flash[:alert] = I18n.t('project.created.failure')

      render 'new', layout: 'logged_layout'
    end
  end

  def show
    validate_entry!(@project)
    @project = TimeDisplayPresenter.new(@project)
    @tasks = @project.tasks.map { |task| TimeDisplayPresenter.new(task) }
  end

  def edit; end

  def update
    validate_ownership!(@project)
    if ProjectUpdater.new(@project).update(project_params)
      flash[:notice] = I18n.t('project.updated.success')

      redirect_to project_path(@project)
    else
      flash[:alert] = I18n.t('project.updated.failure')

      render 'edit'
    end
  end

  def destroy
    validate_ownership!(@project)

    @project.destroy

    redirect_to user_trashes_path(current_user),
                notice: I18n.t('project.deleted.success', name: @project.name)
  end

  private

  def project_params
    params.require(:project)
          .permit(:name, :ending_date, :starting_date)
  end

  def find_project
    @project = Project.find params[:id]
  rescue ActiveRecord::RecordNotFound
    render status: 404, file: 'public/404.html'
  end

  def valid_deadline?
    hash = @project
      .attributes
      .merge(errors: @project.errors, name: 'project')
      .with_indifferent_access

    ValidatorRunner.new(hash, [
      MissingDeadlines,
      StartingDateBeforeToday,
      EndingDateBeforeToday,
      EndingBeforeStarting
    ]).valid?
  end

  def default_layout
    return 'logged_layout' if ['index', 'new'].include? action_name
    'project'
  end
end
