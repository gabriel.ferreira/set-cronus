# frozen_string_literal: true
class TasksController < ApplicationController
  layout :default_layout

  before_action :authenticate_user!
  before_action :find_task, only: [:destroy, :update, :edit]
  before_action :find_project, only: [:new, :create]
  before_action :find_notifications

  def index
    @project = Project.find(params[:project_id])
    validate_entry!(@project)
    @tasks_timeline = @project.tasks.map { |task| [task.name, task.starting_date, task.ending_date] }
    @tasks = @project.tasks.map { |task| TimeDisplayPresenter.new(task) }
  end

  def new
    @task = @project.tasks.build
  end

  def create
    @task = @project.tasks.build tasks_params
    @task.contributors << current_user

    if valid_data_for_creation? && @task.save
      flash[:notice] = I18n.t('task.created.success')
      redirect_to task_path(@task)
    else
      flash[:alert] = I18n.t('task.created.failure')
      render 'new', layout: 'project'
    end
  end

  def show
    @task = Task.find params[:id]
    validate_entry!(@task)

    @task = TimeDisplayPresenter.new(@task)
    @buttons_display = TaskButtonsGenerator.new(task: @task, user: current_user)
  end

  def edit; end

  def update
    parameters = tasks_params.to_h
      .merge(errors: @task.errors, name: 'task', project: @task.project)
      .symbolize_keys

    if valid_data_for_edition?(parameters) && @task.update(tasks_params)
      flash[:notice] = I18n.t('task.updated.success')
      redirect_to task_path(@task)
    else
      flash[:alert] = I18n.t('task.updated.failure')
      render 'edit'
    end
  end

  private

  def find_task
    @task = Task.find(params[:id])
    validate_ownership!(@task)
  end

  def find_project
    @project = Project.find(params[:project_id])
    validate_ownership!(@project)
  end

  def tasks_params
    params.require(:task)
          .permit(:name, :description, :starting_date, :ending_date)
  end

  def valid_data_for_creation?
    hash = @task
      .attributes
      .merge(errors: @task.errors, name: 'task', project: @task.project)
      .with_indifferent_access

    ValidatorRunner.new(hash, [MissingDeadlines,
                               EndingBeforeStarting,
                               StartingDateBeforeToday,
                               StartingBeforeProject,
                               EndingDateBeforeToday]).valid?
  end

  def valid_data_for_edition?(params)
    ValidatorRunner.new(params, [MissingDeadlines,
                               EndingBeforeStarting,
                               StartingBeforeProject,
                               EndingDateBeforeToday]).valid?
  end

  def default_layout
    return 'project' if ['index', 'new'].include? action_name
    'task'
  end
end
