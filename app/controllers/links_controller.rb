# frozen_string_literal: true
class LinksController < ApplicationController
  layout 'project'

  before_action :authenticate_user!
  before_action :find_project, only: [:new, :create, :unlink]
  before_action :find_notifications

  def new
    @errors ||= []
    group_tasks_for_selection
  end

  def create
    @requisite = find_task(params[:task_requisite])
    @dependency = find_task(params[:task_dependency])

    if @requisite.blank? || @dependency.blank?
      flash[:alert] = I18n.t('task.link.errors.message')

      group_tasks_for_selection
      render :new
    else
      @validator = LinksValidator.new(@requisite, @dependency)

      successfull = LinkCreator.new(requisite: @requisite,
                                    user: current_user,
                                    dependency: @dependency,
                                    validator: @validator).create
      if successfull
        redirect_to project_tasks_path(@project)
      else
        @errors = @validator.errors
        flash[:alert] = I18n.t('task.link.errors.message')

        group_tasks_for_selection
        render :new
      end
    end
  end

  def unlink
    @task = Task.find(params[:id])
    @dependencies = @task.dependencies.pluck(:name, :id)
  end

  def destroy
    @task = Task.find(params[:id])
    validate_ownership!(@task)

    if valid_params?(params)
      @task.dependencies.destroy(params[:dependency])

      StateMachines::Work.new(record: @task,
                              user: current_user).ready

      redirect_to project_path(@task.project)
    else
      @project = @task.project
      @dependencies = @task.dependencies.pluck(:name, :id)
      flash[:alert] = I18n.t('task.link.errors.no_dependencies')
      render :unlink
    end
  end

  private

  def group_tasks_for_selection
    @tasks = @project.tasks.pluck(:name, :id)
  end

  def find_task(id)
    @project.tasks.find id
  end

  def find_project
    @project = current_user.projects.find params[:project_id]
    validate_ownership!(@project)
  rescue ActiveRecord::RecordNotFound
    render file: 'public/404.html'
  end

  def valid_params?(params)
    params[:dependency].present?
  end
end
