# frozen_string_literal: true
class UsersController < ApplicationController
  layout 'logged_layout'

  before_action :authenticate_user!
  before_action :find_notifications

  def show
    @user = current_user
  end

  def shared_projects
    @projects = current_user.third_person_projects.map { |project| TimeDisplayPresenter.new(project) }
  end
end
