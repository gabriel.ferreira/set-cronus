class TrashesController < ApplicationController
  layout 'logged_layout'

  before_action :find_notifications, only: [:index]

  def create
    @project = Project.find params[:project_id]

    if @project.update_attributes(in_trash: true)
      flash[:notice] = I18n.t('project.on_trash.success', name: @project.name)
    else
      flash[:error] = I18n.t('project.on_trash.failure', name: @project.name)
    end

    redirect_to user_projects_path(@project.user)
  end

  def index
    @projects = current_user
      .projects
      .where(in_trash: true)
      .map { |project| TimeDisplayPresenter.new(project) }
  end

  def restore
    @project = Project.find params[:project_id]

    if @project.update_attributes(in_trash: false)
      flash[:notice] = I18n.t('project.out_trash.success', name: @project.name)
    else
      flash[:error] = I18n.t('project.out_trash.failure', name: @project.name)
    end

    redirect_to user_trashes_path(@project.user)
  end
end
