class NotificationsController < ApplicationController
  layout 'logged_layout'

  def index
    find_notifications
  end

  def destroy
    @notification = Notification.find(params[:id])

    @notification.update_attributes!(seen: true)

    redirect_to action: 'index'
  end
end
