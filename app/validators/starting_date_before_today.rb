class StartingDateBeforeToday
  def initialize(args)
    @starting_date = args.fetch(:starting_date)
    @errors = args.fetch(:errors)
    @name = args.fetch(:name)
  end

  def validate
    return if @starting_date.blank?

    if @starting_date < Time.zone.today.beginning_of_day
      @errors[:starting_date] << I18n.t("#{@name}.errors.starting_date_before_today")
    end
  end
end