# frozen_string_literal: true
module Links
  class ModelValidator < Base
    def check_invalid_condition
      @task.invalid? || @dependency.invalid?
    end

    def error_message
      I18n.t('task.link.errors.invalid')
    end

    def key
      :model
    end
  end
end
