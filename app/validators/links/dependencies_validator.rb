# frozen_string_literal: true
module Links
  class DependenciesValidator < Base
    def custom_initialization
      @finder = DependencyFinder.new(@dependency)
    end

    def key
      :dependency
    end

    def check_invalid_condition
      @finder.find.include? @task
    end

    def error_message
      I18n.t('task.link.errors.dependencies')
    end
  end
end
