# frozen_string_literal: true
module Links
  class EqualityValidator < Base
    def check_invalid_condition
      @task == @dependency
    end

    def key
      :equality
    end

    def error_message
      I18n.t('task.link.errors.same_record')
    end
  end
end
