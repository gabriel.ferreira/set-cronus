# frozen_string_literal: true
module Links
  class Base
    def initialize(args)
      @task = args.fetch(:task)
      @dependency = args.fetch(:dependency)
      @errors = {}
      custom_initialization
    end

    def errors
      if check_invalid_condition
        @errors[key] ||= []
        @errors[key] << error_message
        @errors[key].flatten!
      end

      @errors
    end

    def error_message
      raise 'Not implemented method'
    end

    def key
      raise 'Not implemented method'
    end

    def check_invalid_condition
      false
    end

    def custom_initialization
    end
  end
end
