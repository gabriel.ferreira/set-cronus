# frozen_string_literal: true
module Links
  class SameProjectValidator < Base
    def check_invalid_condition
      @task.project != @dependency.project
    end

    def error_message
      I18n.t('task.link.errors.different_projects')
    end

    def key
      :project
    end
  end
end
