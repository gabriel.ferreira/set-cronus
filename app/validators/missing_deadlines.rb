class MissingDeadlines
  def initialize(args)
    @ending_date = args.fetch(:ending_date)
    @starting_date = args.fetch(:starting_date)
    @errors = args.fetch(:errors)
    @name = args.fetch(:name)
  end

  def validate
    if @ending_date.blank?
      @errors[:ending_date] << I18n.t("#{@name}.errors.missing_ending_date")
    end

    if @starting_date.blank?
      @errors[:starting_date] << I18n.t("#{@name}.errors.missing_starting_date")
    end
  end
end
