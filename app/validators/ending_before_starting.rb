class EndingBeforeStarting
  def initialize(args)
    @ending_date = convert_string args.fetch(:ending_date)
    @starting_date = convert_string args.fetch(:starting_date)
    @errors = args.fetch(:errors)
    @name = args.fetch(:name)
  end

  def validate
    return if @starting_date.blank? || @ending_date.blank?

    if @ending_date < @starting_date
      @errors[:ending_date] << I18n.t("#{@name}.errors.finish_before_started")
    elsif @ending_date.beginning_of_day == @starting_date.beginning_of_day
      @errors[:ending_date] << I18n.t("#{@name}.errors.start_and_end_in_same_day")
    end
  end

  private

  def convert_string(data)
    return if data.blank?
    return Time.parse(data) if data.is_a?(String)
    data
  end
end
