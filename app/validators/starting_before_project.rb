class StartingBeforeProject
  def initialize(args)
    @project = args.fetch(:project)
    @starting_date = args.fetch(:starting_date)
    @errors = args.fetch(:errors)
  end

  def validate
    if @starting_date < @project.starting_date
      @errors[:starting_date] << I18n.t("task.errors.starting_before_project")
    end
  end
end
