class EndingDateBeforeToday
  def initialize(args)
    @ending_date = args.fetch(:ending_date)
    @errors = args.fetch(:errors)
    @name = args.fetch(:name)
  end

  def validate
    return if @ending_date.blank?

    if @ending_date < Time.zone.today.beginning_of_day
      @errors[:ending_date] << I18n.t("#{@name}.errors.ending_date_before_today")
    end
  end
end
