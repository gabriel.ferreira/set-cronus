class TaskButtonsGenerator
	def initialize(args)
		@task = args.fetch(:task)
		@user = args.fetch(:user)
	end

	def event_buttons
		klass = renderer_klass
		return klass.new(@task).render if klass

	  []
	end

	private

	def renderer_klass
		return multi_user_project_renderer if @task.project.contributors.any?
		return TaskButtons::SingleUserPanel if @task.owner == @user
	end

	def multi_user_project_renderer
		return TaskButtons::ProjectManagerPanel if @task.owner == @user
		return TaskButtons::ContributorPanel if @task.assigned_to?(@user)
	end
end