# frozen_string_literal: true
class TimeDisplayPresenter < SimpleDelegator
  include ActionView::Helpers::DateHelper

  def initialize(resource)
    __setobj__(resource)
    @starting_date = __getobj__.starting_date
    @time_remaining = distance_of_time_in_words(Time.zone.now, __getobj__.ending_date)
  end

  def time_remaining
    I18n.t('time.remaining',
           time: @time_remaining)
  end

  def starting_date
    I18n.t 'time.starting_at',
           time: (I18n.l @starting_date, format: :long)
  end
end
