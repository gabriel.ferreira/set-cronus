module TaskButtons
  class Renderer
    def initialize(events:, id:)
      @events = events
      @id = id
    end

    def render
      @events.map do |event|
        event_route_for(event)
      end
    end

    private

    def event_route_for(event)
      { route: routes.send("task_#{event}_path", @id),
        method: :post,
        id: "task_#{event}",
        name: I18n.t("task.events.#{event}") }
    end

    def routes
      Rails.application.routes.url_helpers
    end
  end
end
