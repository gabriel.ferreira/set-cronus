module TaskButtons
	class ProjectManagerPanel
		include StateMachines::WorkEvents

		def initialize(task)
			@task = task
			@transitions_finder = StateMachines::AllowedTransitions.new(record: @task)
		end

		def render
      Renderer.new(events: filter_allowed_events, id: @task.id).render
		end

		def excluded_events
			[FINISH, VERIFY, READY]
		end

		private

		def filter_allowed_events
			@transitions_finder
        .allowed_transitions
        .find_all { |event| !excluded_events.include?(event) }
		end
	end
end
