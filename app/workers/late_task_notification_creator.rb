class LateTaskNotificationCreator
  include ActionView::Helpers::DateHelper

  def run(tasks)
    Rails.logger.debug("Running late task notifications for #{tasks.size} tasks.")

    tasks.each do |task|
      task.contributors.each do |user|
        description = I18n.t(
          'notification.late_task.description',
          project: task.project.name,
          task: task.name,
          late_time: distance_of_time_in_words(task.ending_date, Time.zone.now)
        )

        Notification.create(
          user: user,
          task: task,
          project: task.project,
          description: description,
          notification_type: 'late_task'
        )
      end
    end

    Rails.logger.debug("Completed successfully.")
  end
end
