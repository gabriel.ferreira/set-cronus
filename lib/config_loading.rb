def load_config(file)
	file = File.join(Rails.root, 'config', file)	
	YAML.load_file(file)
end
