namespace :puma do
  desc 'Start web server on puma'
  task :start do
  	within "/var/www/setcronus/current" do
  		with rails_env: 'production' do
  			execute 'rails s -d'
  		end
  	end
  end
end
