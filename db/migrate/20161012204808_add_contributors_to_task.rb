# frozen_string_literal: true
class AddContributorsToTask < ActiveRecord::Migration[5.0]
  def change
    create_table :task_contributors do |t|
      t.integer :task_id
      t.integer :user_id
    end

    add_index :task_contributors, :task_id
    add_index :task_contributors, :user_id
  end
end
