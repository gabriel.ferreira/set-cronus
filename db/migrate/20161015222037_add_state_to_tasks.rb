# frozen_string_literal: true
class AddStateToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :state, :string, default: 'ready'
  end
end
