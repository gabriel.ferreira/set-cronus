# frozen_string_literal: true
class AddProjectReferenceToTasks < ActiveRecord::Migration[5.0]
  def change
    add_reference :tasks, :project, index: true, foreign_key: true
  end
end
