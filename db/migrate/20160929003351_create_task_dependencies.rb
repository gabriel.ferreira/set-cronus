# frozen_string_literal: true
class CreateTaskDependencies < ActiveRecord::Migration[5.0]
  def change
    create_table :task_dependencies do |t|
      t.integer :task_id
      t.integer :dependency_id
    end
    add_index :task_dependencies, :task_id
    add_index :task_dependencies, :dependency_id
  end
end
