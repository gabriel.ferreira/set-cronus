class AddInTrashToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :in_trash, :boolean, default: false
  end
end
