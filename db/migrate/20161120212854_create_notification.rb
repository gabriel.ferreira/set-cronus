class CreateNotification < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.references :task, foreign_key: true
      t.references :user, foreign_key: true
      t.references :project, foreign_key: true
      t.text :description
      t.string :notification_type
      t.boolean :seen

      t.timestamps
    end
  end
end
