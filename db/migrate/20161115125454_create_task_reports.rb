class CreateTaskReports < ActiveRecord::Migration[5.0]
  def change
    create_table :task_reports do |t|
      t.references :project, foreign_key: true
      t.references :user, foreign_key: true
      t.references :task, foreign_key: true
      t.string :original_state
      t.string :new_state
      t.text :description
    end
  end
end
