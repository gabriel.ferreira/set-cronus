# frozen_string_literal: true
class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.datetime :starting_date
      t.datetime :ending_date

      t.timestamps
    end
  end
end
