# frozen_string_literal: true
class AddContributorsToProject < ActiveRecord::Migration[5.0]
  def change
    create_table :project_contributors do |t|
      t.integer :project_id
      t.integer :user_id
    end

    add_index :project_contributors, :project_id
    add_index :project_contributors, :user_id
  end
end
