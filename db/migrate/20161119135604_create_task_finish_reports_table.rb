class CreateTaskFinishReportsTable < ActiveRecord::Migration[5.0]
  def change
    create_table :task_finish_reports do |t|
      t.references :project, foreign_key: true
      t.references :user, foreign_key: true
      t.references :task, foreign_key: true
      t.integer :delayed_time
    end
  end
end
