class AddDefaultToNotificationSeenColumn < ActiveRecord::Migration[5.0]
  def change
    change_column :notifications, :seen, :boolean, :default => false
  end
end
