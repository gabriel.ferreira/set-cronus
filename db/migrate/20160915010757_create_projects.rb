# frozen_string_literal: true
class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.datetime :starting_date
      t.datetime :ending_date

      t.timestamps
    end
  end
end
