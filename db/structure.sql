--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: project_contributors; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE project_contributors (
    id integer NOT NULL,
    project_id integer,
    user_id integer
);


--
-- Name: project_contributors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE project_contributors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: project_contributors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE project_contributors_id_seq OWNED BY project_contributors.id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE projects (
    id integer NOT NULL,
    name character varying,
    starting_date timestamp without time zone,
    ending_date timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer,
    in_trash boolean DEFAULT false
);


--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE projects_id_seq OWNED BY projects.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: task_contributors; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE task_contributors (
    id integer NOT NULL,
    task_id integer,
    user_id integer
);


--
-- Name: task_contributors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE task_contributors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_contributors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE task_contributors_id_seq OWNED BY task_contributors.id;


--
-- Name: task_dependencies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE task_dependencies (
    id integer NOT NULL,
    task_id integer,
    dependency_id integer
);


--
-- Name: task_dependencies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE task_dependencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_dependencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE task_dependencies_id_seq OWNED BY task_dependencies.id;


--
-- Name: task_finish_reports; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE task_finish_reports (
    id integer NOT NULL,
    project_id integer,
    user_id integer,
    task_id integer,
    delayed_time integer
);


--
-- Name: task_finish_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE task_finish_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_finish_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE task_finish_reports_id_seq OWNED BY task_finish_reports.id;


--
-- Name: task_reports; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE task_reports (
    id integer NOT NULL,
    project_id integer,
    user_id integer,
    task_id integer,
    original_state character varying,
    new_state character varying,
    description text
);


--
-- Name: task_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE task_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE task_reports_id_seq OWNED BY task_reports.id;


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tasks (
    id integer NOT NULL,
    name character varying,
    description text,
    starting_date timestamp without time zone,
    ending_date timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    project_id integer,
    state character varying DEFAULT 'ready'::character varying,
    started_at timestamp without time zone,
    finished_at timestamp without time zone
);


--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tasks_id_seq OWNED BY tasks.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY project_contributors ALTER COLUMN id SET DEFAULT nextval('project_contributors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY projects ALTER COLUMN id SET DEFAULT nextval('projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_contributors ALTER COLUMN id SET DEFAULT nextval('task_contributors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_dependencies ALTER COLUMN id SET DEFAULT nextval('task_dependencies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_finish_reports ALTER COLUMN id SET DEFAULT nextval('task_finish_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_reports ALTER COLUMN id SET DEFAULT nextval('task_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks ALTER COLUMN id SET DEFAULT nextval('tasks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: project_contributors_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_contributors
    ADD CONSTRAINT project_contributors_pkey PRIMARY KEY (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: task_contributors_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY task_contributors
    ADD CONSTRAINT task_contributors_pkey PRIMARY KEY (id);


--
-- Name: task_dependencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY task_dependencies
    ADD CONSTRAINT task_dependencies_pkey PRIMARY KEY (id);


--
-- Name: task_finish_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY task_finish_reports
    ADD CONSTRAINT task_finish_reports_pkey PRIMARY KEY (id);


--
-- Name: task_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY task_reports
    ADD CONSTRAINT task_reports_pkey PRIMARY KEY (id);


--
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_project_contributors_on_project_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_project_contributors_on_project_id ON project_contributors USING btree (project_id);


--
-- Name: index_project_contributors_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_project_contributors_on_user_id ON project_contributors USING btree (user_id);


--
-- Name: index_projects_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_projects_on_user_id ON projects USING btree (user_id);


--
-- Name: index_task_contributors_on_task_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_contributors_on_task_id ON task_contributors USING btree (task_id);


--
-- Name: index_task_contributors_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_contributors_on_user_id ON task_contributors USING btree (user_id);


--
-- Name: index_task_dependencies_on_dependency_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_dependencies_on_dependency_id ON task_dependencies USING btree (dependency_id);


--
-- Name: index_task_dependencies_on_task_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_dependencies_on_task_id ON task_dependencies USING btree (task_id);


--
-- Name: index_task_finish_reports_on_project_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_finish_reports_on_project_id ON task_finish_reports USING btree (project_id);


--
-- Name: index_task_finish_reports_on_task_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_finish_reports_on_task_id ON task_finish_reports USING btree (task_id);


--
-- Name: index_task_finish_reports_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_finish_reports_on_user_id ON task_finish_reports USING btree (user_id);


--
-- Name: index_task_reports_on_project_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_reports_on_project_id ON task_reports USING btree (project_id);


--
-- Name: index_task_reports_on_task_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_reports_on_task_id ON task_reports USING btree (task_id);


--
-- Name: index_task_reports_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_task_reports_on_user_id ON task_reports USING btree (user_id);


--
-- Name: index_tasks_on_project_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tasks_on_project_id ON tasks USING btree (project_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: fk_rails_02e851e3b7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT fk_rails_02e851e3b7 FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: fk_rails_06f69ddf6c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_finish_reports
    ADD CONSTRAINT fk_rails_06f69ddf6c FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_555c73fc30; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_reports
    ADD CONSTRAINT fk_rails_555c73fc30 FOREIGN KEY (task_id) REFERENCES tasks(id);


--
-- Name: fk_rails_8b77d31abb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_finish_reports
    ADD CONSTRAINT fk_rails_8b77d31abb FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: fk_rails_9bfd5a0269; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_reports
    ADD CONSTRAINT fk_rails_9bfd5a0269 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_b872a6760a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT fk_rails_b872a6760a FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_d554e14870; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_reports
    ADD CONSTRAINT fk_rails_d554e14870 FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: fk_rails_ed50d21e9f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY task_finish_reports
    ADD CONSTRAINT fk_rails_ed50d21e9f FOREIGN KEY (task_id) REFERENCES tasks(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20160910200537'), ('20160910211448'), ('20160915010757'), ('20160918185043'), ('20160924202757'), ('20160924210229'), ('20160929003351'), ('20161009175558'), ('20161012204808'), ('20161015222037'), ('20161023181630'), ('20161030183303'), ('20161113150135'), ('20161115125454'), ('20161119135604');


